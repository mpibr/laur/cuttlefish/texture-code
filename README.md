# texture-code

Extracting skin-pattern representation from low-resolution imaging of cuttlefish.

Code for extracting chromatophore sizes using from high-resolution imaging data can be found at ![https://gitlab.mpcdf.mpg.de/mpibr/laur/cuttlefish/cuttlefish-code-public](https://gitlab.mpcdf.mpg.de/mpibr/laur/cuttlefish/cuttlefish-code-public).

## Publication

> Woo, T.\*, Liang, X.\*, Evans, D.A. et al. The dynamics of pattern matching in camouflaging cuttlefish. Nature (2023). https://doi.org/10.1038/s41586-023-06259-2 (![Analysis code](https://gitlab.mpcdf.mpg.de/mpibr/laur/cuttlefish/camo-pattern-dynamics))

## Usage

See https://gitlab.mpcdf.mpg.de/mpibr/laur/cuttlefish/camo-pattern-dynamics or 

```
./get_activations.py --help
```
