import numpy as np
import skimage.transform
import cv2
from scipy import spatial
import math
import matplotlib.pyplot as plt
from scipy.spatial.distance import pdist, squareform
from scipy import signal
import pandas as pd
from sklearn import preprocessing
from sklearn.decomposition import PCA
import random
from tqdm import tqdm
import joblib
import random
import imageio
from skimage import morphology
import scipy.ndimage
import os
from mpl_toolkits.mplot3d import Axes3D
import h5py
import matplotlib.collections as mcoll
import matplotlib.path as mpath


def bandpass_filter(series, low=200.0, high=4000.0):
    dt = series.index[1] - series.index[0]
    fs_nyquist = (1.0/dt) / 2.0
    if low < 0.1:
        # Lowpass filter only.
        bf, af = signal.butter(2, high/fs_nyquist, btype='lowpass')
    elif high >= 10000:
        # Highpass filter only.
        bf, af = signal.butter(2, low/fs_nyquist, btype='highpass')
    else:
        bf, af = signal.butter(2, (low/fs_nyquist, high/fs_nyquist),
                               btype='bandpass')
    return pd.Series(signal.filtfilt(bf, af, series).astype(np.float32),
                     index=series.index)


def predict_img(img, clf, windowSize):
    BigImg = np.expand_dims(img, axis=0)
    paddedW = int(np.floor(img.shape[0] / windowSize) + 1) * windowSize
    paddedH = int(np.floor(img.shape[1] / windowSize) + 1) * windowSize
    BigImg = np.uint8(128 * np.ones([1, paddedW, paddedH, 3]))  # don't want it to look like a chromatophore
    BigImg[:, 0:img.shape[0], 0:img.shape[1], :] = img

    # save the full prediction as uint 8
    pred = np.uint8(np.squeeze(clf.predict(BigImg))[:, :, :-1] * 255)

    return pred[:img.shape[0],:img.shape[1]]


def find_the_large_mask(input_labeled_image, input_intensity_image):
    # count = 0

    prop_rmask = skimage.measure.regionprops(input_labeled_image, intensity_image=input_intensity_image)
    mark = prop_rmask[0]
    max_area = prop_rmask[0].area
    for region in prop_rmask:
        if region.area > max_area:
            # count = count +1
            max_area = region.area
            mark = region

    return (mark)


def hogfeature(img,cell,block):
    cell_size = (cell, cell)  # h x w in pixels
    block_size = (block, block)  # h x w in cells
    nbins = 9  # number of orientation bins
    hog = cv2.HOGDescriptor(_winSize=(img.shape[1] // cell_size[1] * cell_size[1],
                                      img.shape[0] // cell_size[0] * cell_size[0]),
                            _blockSize=(block_size[1] * cell_size[1],
                                        block_size[0] * cell_size[0]),
                            _blockStride=(cell_size[1], cell_size[0]),
                            _cellSize=(cell_size[1], cell_size[0]),
                            _nbins=nbins)
    n_cells = (img.shape[0] // cell_size[0], img.shape[1] // cell_size[1])
    hog_feats = hog.compute(img) \
        .reshape(n_cells[1] - block_size[1] + 1,
                 n_cells[0] - block_size[0] + 1,
                 block_size[0], block_size[1], nbins) \
        .transpose((1, 0, 2, 3, 4))
    # hog_feats now contains the gradient amplitudes for each direction,
    # for each cell of its group for each group. Indexing is by rows then columns.

    gradients = np.zeros((n_cells[0], n_cells[1], nbins))

    # count cells (border cells appear less often across overlapping groups)
    cell_count = np.full((n_cells[0], n_cells[1], 1), 0, dtype=int)

    for off_y in range(block_size[0]):
        for off_x in range(block_size[1]):
            gradients[off_y:n_cells[0] - block_size[0] + off_y + 1,
            off_x:n_cells[1] - block_size[1] + off_x + 1] += \
                hog_feats[:, :, off_y, off_x, :]
            cell_count[off_y:n_cells[0] - block_size[0] + off_y + 1,
            off_x:n_cells[1] - block_size[1] + off_x + 1] += 1

    # Average gradients
    gradients /= cell_count
    return(gradients)


def rgb2gray(rgb):
    return np.dot(rgb[...,:3], [0.2989, 0.5870, 0.1140])


def brighter(img,value=30):
    img = img.astype(np.uint8)
    hsv = cv2.cvtColor(img, cv2.COLOR_RGB2HSV)
    h, s, v = cv2.split(hsv)
    value = 50-int(v[v>0].min())
    lim = 255 - value
    v[v > lim] = 255
    v[v <= lim] += value
    v[v <= value] -= value
    final_hsv = cv2.merge((h, s, v))
    img = cv2.cvtColor(final_hsv, cv2.COLOR_HSV2RGB)
    return img


def cropImage(img,mask,BATCH_WIDTH,BATCH_HEIGHT):
    width  = img.shape[0]
    height = img.shape[1]
    while 1:
        x = random.randrange(0, width - BATCH_WIDTH)
        y = random.randrange(0, height - BATCH_HEIGHT)
        image1 = img[x:(x + BATCH_WIDTH), y:(y + BATCH_HEIGHT),:]
        label1 = mask[x:(x + BATCH_WIDTH), y:(y + BATCH_HEIGHT)]
        if label1.all():
            break
    # return center_reg(image1,BATCH_WIDTH*2)
    return image1


def preprocess_image(img,img_width=224,img_height=224):
    from tensorflow.keras.applications import vgg19
    from keract import get_activations

    # from keract import get_activations
    from tensorflow.keras.preprocessing.image import load_img, img_to_array
    img = img_to_array(img)
    img = cv2.resize(img, dsize=(img_width,img_height), interpolation=cv2.INTER_NEAREST)

    # img= rgb2gray(img)
    # img=img.astype(np.uint8)
    # img = cv2.equalizeHist(img)
    # # # # img = img / (img.max() / 255)
    # # # # img = img / (img[img != 0].mean() / 100)
    # #
    if len(img.shape)==2:
        img = img.reshape((img.shape[0], img.shape[1], 1))
        img=np.concatenate((img, img,img),axis=2)
    #
    # img=img.astype(np.uint8)
    # img_yuv = cv2.cvtColor(img, cv2.COLOR_RGB2YUV)
    # img_yuv[:, :, 0] = cv2.equalizeHist(img_yuv[:, :, 0])
    # img = cv2.cvtColor(img_yuv, cv2.COLOR_YUV2RGB)
    # img = brighter(img)

    img = img.reshape((1, img.shape[0], img.shape[1], img.shape[2]))
    img = img.astype('float32')
    img = vgg19.preprocess_input(img)
    return img


def histN(img):
    img= rgb2gray(img)
    img=img.astype(np.uint8)
    img = cv2.equalizeHist(img)
    img = img.reshape((img.shape[0], img.shape[1], 1))
    img = np.concatenate((img, img, img), axis=2)
    return img


def preprocess_image2(img,img_width=128,img_height=128,n=30):
    from tensorflow.keras.applications import vgg19
    from keract import get_activations
    from tensorflow.keras.preprocessing.image import load_img, img_to_array
    masked = img[np.any(img, axis=(1,2)),:,:]
    masked = masked[:,np.any(masked, axis=(0,2)), :]
    mask = masked.sum(axis=2) > 0
    image = []
    for i in range(n):
        image.append(cropImage(masked,mask,img_width,img_height))
    image = np.array(image)
    img = image.astype('float32')
    img = vgg19.preprocess_input(img)
    return img


def gram_matrix(x,dims):
    features = np.reshape(np.transpose(x,(2,0,1)),(dims[2],dims[0]*dims[1]))
    gram = np.dot(features, np.transpose(features))
    return gram


def style_rep(style):
    style=np.squeeze(style)
    dims = style.shape
    S = gram_matrix(style,dims)
    size=dims[0]*dims[1]*dims[2]
    return S / (size ** 2)  #think about this normalization some more. Now it's to sort of match the texture synthesis


def to_tex(img,model,sz=224):
    from tensorflow.keras.applications import vgg19
    from keract import get_activations
    from tensorflow.keras.preprocessing.image import load_img, img_to_array
    LAYER = 'block5_conv1'
    currImg = preprocess_image(img,sz,sz)
    # model.compile(loss="categorical_crossentropy", optimizer="adam")
    activations = get_activations(model, currImg, layer_name=LAYER, auto_compile=True)[LAYER]#.popitem(True)[1]
    # activations = list(get_activations(model, currImg).values())
 #   first = style_rep(activations[2])
  #  second = style_rep(activations[5])
 #   third = style_rep(activations[8])
 #    fourth = style_rep(activations[13])
 #    fifth = style_rep(activations[18]).max(axis=0)
 #    fifth = style_rep(activations[LAYER])
    fifth = activations.max(axis=(1,2))

    # plt.figure(figsize=(8, 8))
    # n=0
    # for i in range(8):
    #     for j in range(8):
    #         n=n+1
    #         plt.subplot(8,8,n)
    #         plt.imshow(activations[18][0,:,:,n-1])
    #         plt.axis('off')

    # vggRep=np.append(fifth.ravel(),fourth.ravel())
    # vggRep = fifth.ravel()
    vggRep = fifth.ravel()
    return vggRep


def to_tex2(img,model,sz=224):
    LAYER = 'block5_conv1'
    currImg = preprocess_image2(img,sz,sz,n=5)
    activations = get_activations(model, currImg, layer_name=LAYER, auto_compile=True).popitem(True)[1]
    # fifth = style_rep(activations)
    fifth = activations.max(axis=(0,1,2))
    # fifth = activations.max(axis=(1,2))

    # vggRep = fifth.mean(axis=0).ravel()
    vggRep = fifth.ravel()

    return vggRep


def fish_mask(img,clf):
    img2 = img[::3, ::3, :]
    seg = predict_img(img2, clf, 256)[:, :, 0]
    # kernel_size=75
    # blur=cv2.GaussianBlur(seg, (kernel_size, kernel_size), 0)
    seg = cv2.resize(seg, dsize=(img.shape[0], img.shape[1]), interpolation=cv2.INTER_AREA)
    seg2 = seg > 50
    labels = skimage.measure.label(seg2, connectivity=2)
    props = find_the_large_mask(labels, seg)
    mask = props.filled_image
    crop1 = img[props._slice]
    masked = np.copy(crop1)
    for i in range(0, 3):
        masked[:, :, i] = masked[:, :, i] * mask
    return masked, props


def center_reg(img,windowsize):
    if len(img.shape)<3:
        img = np.expand_dims(img, axis=2)
    masked = img  # * mask
    masked = masked[:, np.any(img, axis=0)[:, 0]]
    img = masked[np.any(img, axis=1)[:, 0], :]

    x = (windowsize - img.shape[0]) // 2
    y = (windowsize - img.shape[1]) // 2
    reg = np.zeros((windowsize,windowsize,img.shape[2]),dtype=img.dtype)
    reg[x:(x + img.shape[0]), y:(y + img.shape[1]), :] = img
    return np.squeeze(reg)


def center_rotated(img,props,windowsize):
    if len(img.shape)<3:
        img = np.expand_dims(img, axis=2)
    x = (windowsize - img.shape[0]) // 2
    y = (windowsize - img.shape[1]) // 2
    reg = np.zeros((windowsize,windowsize,img.shape[2]),dtype=img.dtype)
    reg[x:(x + img.shape[0]), y:(y + img.shape[1]), :] = img

    y0, x0 = np.round(props.centroid).astype(int)
    yw, xw = props.weighted_centroid
    orientation = props.orientation
    if orientation>0:
        M = cv2.getRotationMatrix2D((windowsize / 2, windowsize / 2), -orientation / math.pi * 180, 1)
        # rotated = skimage.transform.rotate(reg, -orientation / math.pi * 180, resize=False)
    else:
        M = cv2.getRotationMatrix2D((windowsize / 2, windowsize / 2), -orientation / math.pi * 180 + 180, 1)
        # rotated = skimage.transform.rotate(reg, -orientation / math.pi * 180 + 180, resize=False)
    rotated = cv2.warpAffine(reg, M, (windowsize, windowsize))

    # if flip and (xw > x0):
    #     # angle = -orientation / math.pi * 180
    #     rotated = skimage.transform.rotate(reg, -orientation / math.pi * 180, resize=False)
    # else:
    #     # angle = -orientation / math.pi * 180 + 180
    #     rotated = skimage.transform.rotate(reg, -orientation / math.pi * 180 + 180, resize=False)
    # rotated=np.array(rotated * 255, dtype=np.uint8)
    # M = cv2.getRotationMatrix2D((windowsize / 2, windowsize / 2), angle, 1)
    # rotated = cv2.warpAffine(reg, M, (windowsize, windowsize))
    return np.squeeze(rotated)


def rigid_reg(img1,img0,flip=1,trim_sz = 256):
    warp_flip = 0
    if len(img1.shape)==3:
        img1_gray = cv2.cvtColor(img1, cv2.COLOR_BGR2GRAY)
        img0_gray = cv2.cvtColor(img0, cv2.COLOR_BGR2GRAY)
    else:
        img1_gray = img1
        img0_gray = img0
    sz = img0.shape
    # trim_sz=scale
    scale = sz[0]/trim_sz
    img0_gray = cv2.resize(img0_gray, (trim_sz, trim_sz))
    img1_gray = cv2.resize(img1_gray, (trim_sz, trim_sz))


    warp_mode = cv2.MOTION_EUCLIDEAN
    warp_matrix1 = np.eye(2, 3, dtype=np.float32)
    warp_matrix2 = np.eye(2, 3, dtype=np.float32)
    number_of_iterations=1000
    termination_eps = 1e-12
    criteria = (cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, number_of_iterations, termination_eps)
    (cc1, warp_matrix1) = cv2.findTransformECC (img0_gray,img1_gray,warp_matrix1, warp_mode, criteria,None,1)
    # M = cv2.getRotationMatrix2D((sz[0] / 2, sz[1] / 2), 180, 1)
    # img1_gray2 = cv2.warpAffine(img1_gray, M, (sz[0], sz[1]))
    M = cv2.getRotationMatrix2D((trim_sz / 2, trim_sz / 2), 180, 1)
    img1_gray2 = cv2.warpAffine(img1_gray, M, (trim_sz, trim_sz))
    (cc2, warp_matrix2) = cv2.findTransformECC (img0_gray,img1_gray2,warp_matrix2, warp_mode, criteria,None,1)

    if flip and cc2>cc1:
        warp_matrix = warp_matrix2
        warp_matrix[:, 2] = warp_matrix[:, 2] * scale
        M = cv2.getRotationMatrix2D((sz[0] / 2, sz[1] / 2), 180, 1)
        # warp_matrix = np.matmul(np.vstack([warp_matrix, [0, 0, 1]]) , np.vstack([M, [0, 0, 1]]))
        # img1_aligned = cv2.warpAffine(img1, warp_matrix[:2,:], (sz[1], sz[0]), flags=cv2.INTER_LINEAR + cv2.WARP_INVERSE_MAP)

        img2 = cv2.warpAffine(img1, M, (sz[0], sz[1]))
        img1_aligned = cv2.warpAffine(img2, warp_matrix, (sz[1], sz[0]), flags=cv2.INTER_LINEAR + cv2.WARP_INVERSE_MAP)
        # img1_aligned = cv2.warpAffine(img1_gray2, warp_matrix, (trim_sz, trim_sz), flags=cv2.INTER_LINEAR + cv2.WARP_INVERSE_MAP)
        warp_flip = 1

    else:
        warp_matrix = warp_matrix1
        warp_matrix[:, 2] = warp_matrix[:, 2] * scale
        img1_aligned = cv2.warpAffine(img1, warp_matrix, (sz[1], sz[0]), flags=cv2.INTER_LINEAR + cv2.WARP_INVERSE_MAP)
        # img1_aligned = cv2.warpAffine(img1_gray, warp_matrix, (trim_sz, trim_sz), flags=cv2.INTER_LINEAR + cv2.WARP_INVERSE_MAP)

    # plt.figure()
    # plt.subplot(311)
    # plt.imshow(img0_gray)
    # plt.subplot(312)
    # plt.imshow(img1_gray)
    # plt.subplot(313)
    # plt.imshow(img1_aligned)

    return img1_aligned, warp_matrix


def rigid_reg2(img1,img0,flip=1,trim_sz = 256):
    warp_flip = 0
    if len(img1.shape)==3:
        img1_gray = cv2.cvtColor(img1, cv2.COLOR_BGR2GRAY)
        img0_gray = cv2.cvtColor(img0, cv2.COLOR_BGR2GRAY)
    else:
        img1_gray = img1
        img0_gray = img0
    sz = img0.shape
    # trim_sz=scale
    scale = sz[0]/trim_sz
    img0_gray = cv2.resize(img0_gray, (trim_sz, trim_sz))
    img1_gray = cv2.resize(img1_gray, (trim_sz, trim_sz))


    warp_mode = cv2.MOTION_EUCLIDEAN
    warp_matrix1 = np.eye(2, 3, dtype=np.float32)
    warp_matrix2 = np.eye(2, 3, dtype=np.float32)
    number_of_iterations=1000
    termination_eps = 1e-12
    criteria = (cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, number_of_iterations, termination_eps)
    (cc1, warp_matrix1) = cv2.findTransformECC (img0_gray,img1_gray,warp_matrix1, warp_mode, criteria,None,1)
    # M = cv2.getRotationMatrix2D((sz[0] / 2, sz[1] / 2), 180, 1)
    # img1_gray2 = cv2.warpAffine(img1_gray, M, (sz[0], sz[1]))
    M = cv2.getRotationMatrix2D((trim_sz / 2, trim_sz / 2), 180, 1)
    img1_gray2 = cv2.warpAffine(img1_gray, M, (trim_sz, trim_sz))
    (cc2, warp_matrix2) = cv2.findTransformECC (img0_gray,img1_gray2,warp_matrix2, warp_mode, criteria,None,1)

    if flip and cc2>cc1:
        warp_matrix = warp_matrix2
        warp_matrix[:, 2] = warp_matrix[:, 2] * scale
        M = cv2.getRotationMatrix2D((sz[0] / 2, sz[1] / 2), 180, 1)
        # warp_matrix = np.matmul(np.vstack([warp_matrix, [0, 0, 1]]) , np.vstack([M, [0, 0, 1]]))
        # img1_aligned = cv2.warpAffine(img1, warp_matrix[:2,:], (sz[1], sz[0]), flags=cv2.INTER_LINEAR + cv2.WARP_INVERSE_MAP)

        img2 = cv2.warpAffine(img1, M, (sz[0], sz[1]))
        img1_aligned = cv2.warpAffine(img2, warp_matrix, (sz[1], sz[0]), flags=cv2.INTER_LINEAR + cv2.WARP_INVERSE_MAP)
        # img1_aligned = cv2.warpAffine(img1_gray2, warp_matrix, (trim_sz, trim_sz), flags=cv2.INTER_LINEAR + cv2.WARP_INVERSE_MAP)
        warp_flip = 1

    else:
        warp_matrix = warp_matrix1
        warp_matrix[:, 2] = warp_matrix[:, 2] * scale
        img1_aligned = cv2.warpAffine(img1, warp_matrix, (sz[1], sz[0]), flags=cv2.INTER_LINEAR + cv2.WARP_INVERSE_MAP)
        # img1_aligned = cv2.warpAffine(img1_gray, warp_matrix, (trim_sz, trim_sz), flags=cv2.INTER_LINEAR + cv2.WARP_INVERSE_MAP)

    # plt.figure()
    # plt.subplot(311)
    # plt.imshow(img0_gray)
    # plt.subplot(312)
    # plt.imshow(img1_gray)
    # plt.subplot(313)
    # plt.imshow(img1_aligned)

    return img1_aligned, warp_matrix,warp_flip

def warp_flip(img0,warp_matrix):
    sz = img0.shape
    M = cv2.getRotationMatrix2D((sz[0] / 2, sz[1] / 2), 180, 1)
    img2 = cv2.warpAffine(img0, M, (sz[0], sz[1]))
    img1_aligned = cv2.warpAffine(img2, warp_matrix, (sz[1], sz[0]), flags=cv2.INTER_LINEAR + cv2.WARP_INVERSE_MAP)
    return img1_aligned

def filp_decicion(img1,img2,model):
    img2_aligned, warp, warp_flip = rigid_reg2(img2, img1, flip=1)
    vecRep1 = to_tex(img1, model, 224)
    vecRep2_o = to_tex(img2, model, 224)
    vecRep2 = to_tex(img2_aligned, model, 224)
    distance_org = np.linalg.norm(vecRep2_o - vecRep1)
    distance = np.linalg.norm(vecRep2 - vecRep1)
    if distance<distance_org and warp_flip:
        return 1,warp
    else:
        return 0,warp

# cv2.estimateRigidTransform(img0_gray,img1_gray)


def get_frame(n):
    global output, f_v_list, frame_n
    cap = cv2.VideoCapture(output + f_v_list[n])
    cap.set(cv2.CAP_PROP_POS_FRAMES, frame_n[n])
    succ, img = cap.read()
    img = np.array(img)[..., ::-1]
    img = histN(img)
    img = apply_mask(img, img, no_mask=1, resize=0, fill=0)
    return img


def plot_atlas(embedding,reg,factor = 1, trace =1):
    # if not reg == None:
    # if len(reg.shape)<4:
    #     reg = np.expand_dims(reg, 3)

    # average = np.mean(reg, axis=0)
    # mask = average.mean(axis=2) > 10
    # mask = np.expand_dims(mask, axis=2)
    xmin = embedding[:, 0].min() - 1
    xmax = embedding[:, 0].max() + 1
    ymin = embedding[:, 1].min() - 1
    ymax = embedding[:, 1].max() + 1
    # factor = 1

    nrow=np.int((xmax-xmin)//factor)
    ncol=np.int((ymax-ymin)//factor)
    if len(reg.shape) < 4:
        img_big = np.zeros((400*ncol,400*nrow),dtype=np.uint8)
    else:
        img_big = np.zeros((400*ncol,400*nrow,reg.shape[3]),dtype=np.uint8)


    for i in range(nrow):
        for j in range(ncol):
            xcenter=xmin+i*factor
            ycenter=ymin+j*factor
            try:
                exp=np.where(np.logical_and(np.logical_and(embedding[:, 0] >= xcenter,
                                                       embedding[:, 0] <= xcenter+factor),
                                        np.logical_and(embedding[:, 1] >= ycenter,
                                                       embedding[:, 1] <= ycenter+factor)))#[0][0]
                if len(exp[0])>1:
                    points = embedding[exp[0], :]
                    center = np.linalg.norm(points - points.mean(axis=0), axis=1).argmin()
                    exp_c=np.where(embedding[:, 1] == points[center, 1])[0][0]
                else:
                    exp_c = exp[0][0]
                # img = np.array(Image.open(imagedir[exp]))
                # if reg == None:
                #     masked = get_frame(exp_c)

                # else:
                img = reg[exp_c,...]
                # masked = img * mask
                # img = brighter(img)

                img1 = cv2.resize(img,(400,400))

                # img1=img1[::3,::3,:]
                if len(reg.shape) < 4:
                    img_big[j*400:(j+1)*400,i*400:(i+1)*400]=img1
                else:
                    img_big[j*400:(j+1)*400,i*400:(i+1)*400,:]=img1

            except:
                continue

    fig = plt.figure(figsize=(nrow+1,ncol))
#    fig.patch.set_facecolor('black')
    ax=plt.subplot()
    if len(reg.shape) < 4:
        ax.imshow(img_big,origin='lower',cmap='gray')
    else:
        ax.imshow(img_big,origin='lower')

    if trace:
        cm = plt.cm.get_cmap('RdYlBu')
        LABELS = range(embedding.shape[0])
        all=ax.scatter((embedding[:, 0]-xmin)*400/factor, (embedding[:, 1]-ymin)*400/factor,
                       vmin=0, vmax=embedding.shape[0], s=5, c=LABELS, cmap=cm,alpha=0.2)
        ax.plot((embedding[:, 0]-xmin)*400/factor, (embedding[:, 1]-ymin)*400/factor, linewidth=.10, c='w')
        ax.set_xlim(0, (xmax - xmin) * 400 / factor)
        ax.set_ylim(0, (ymax - ymin) * 400 / factor)

    ax.set_facecolor("black")
    ax.get_xaxis().set_visible(False)
    ax.get_yaxis().set_visible(False)


    return img_big

    # plt.colorbar(all,ax=ax)
    # cax = plt.colorbar(all, ax=ax, ticks=np.unique(LABELS))
    # cax.set_label('square size (cm)', fontsize=18)
    # cax.ax.set_yticklabels(['{:.2f}'.format(x) for x in 2 ** np.unique(LABELS)], fontsize=8)

    # plt.savefig(output + file_name[0][:-4] + 'skip' + str(skip) +
    #         '-trace-Umap'+str(n_neighbors)+'-'+str(min_dist)+'.png')


def apply_mask(img,mask,no_mask=0,resize=1,fill=0):
    if len(img.shape)<3:
        img = np.expand_dims(img, axis=2)
    if no_mask:
        masked = img #* mask
        masked = masked[:, np.any(img, axis=1)[:,0]]
        masked = masked[np.any(img, axis=1)[:,0], :]
    else:
        if len(mask.shape) < 3:
            mask = np.expand_dims(mask, axis=2)
        masked = img * mask
        masked = masked[:, np.any(mask, axis=0)[:,0]]
        masked = masked[np.any(mask, axis=1)[:,0], :]
    if resize==1:
        w = masked.shape[0]
        l = masked.shape[1]
        img_big = np.zeros((w * 2, w * 2, masked.shape[2]), dtype=masked.dtype)
        w1 = int(w // 2)
        l1 = w-int(l // 2)
        img_big[w1:(w1 + w), l1:(l1 + l), :] = masked
        img_big[img_big.sum(axis=2)==0]=fill
        masked=img_big
    return masked


def get_background(img,props):
    window=500
    bg = np.empty((4,window,window,3),dtype=img.dtype)
    try:
        bg[0,...] = img[(props.bbox[0]-window):props.bbox[0],props.bbox[1]:(props.bbox[1]+window)]
        bg[1,...] = img[(props.bbox[2]):(props.bbox[2]+window),props.bbox[1]:(props.bbox[1]+window)]
        bg[2,...] = img[(props.bbox[0]):(props.bbox[0]+window),(props.bbox[1]-window):props.bbox[1]]
        bg[3,...] = img[(props.bbox[0]):(props.bbox[0]+window), (props.bbox[3]):(props.bbox[3]+window)]
    except:
        pass
    return bg


def match_backgroung(background,model,means):
    brighter=background.mean(axis=(1, 2, 3)).argmax()
    vecRep_text2 = to_tex(background[brighter,...], model)
    vecRep_text2=np.expand_dims(vecRep_text2,axis=0)
    # comb = np.append(means,vecRep_text2,axis=1)
    comb = np.concatenate((means, vecRep_text2), axis=0)
    distance_mat2 = squareform(pdist(comb))
    i = (distance_mat2[30:,:30]).argmin()
    return i,brighter,vecRep_text2






    # vecRep_text2=[]
    # for k in range(4):
    #     vecRep_text2.append(to_tex(background[k,...], model))
    # vecRep_text2 = np.array(vecRep_text2)
    # comb = np.concatenate((means, vecRep_text2), axis=0)
    # distance_mat2 = squareform(pdist(comb))
    # #
    # #
    # for k in range(4):
    #     plt.subplot(3,2,k+1)
    #     plt.imshow(background[k,...])
    #     plt.axis('off')
    # ax=plt.subplot(3, 2, 5)
    # # fig, ax = plt.subplots(figsize=(3, 3))
    # iax = ax.imshow(distance_mat2, origin='top')
    # plt.xticks(rotation='vertical')
    # ax.set_xticks(np.arange(distance_mat2.shape[1]))
    # ax.set_xticklabels(np.arange(distance_mat2.shape[1]))
    # print(distance_mat2[30:,:30].argmin(axis=1))
    #
    #
    # plt.subplot(3, 2, 6)
    # file = texture_dir + 'imgCrop' + str(i) + '.png'
    # img = Image.open(file)
    # plt.imshow(img)


def PCA_plot(vecRep,d1=0,d2=1,n=20):
    texMat=preprocessing.scale(vecRep)
    pca = PCA(n_components=n,svd_solver='full')
    texPCA=pca.fit(texMat)
    embedding=texPCA.transform(texMat)
    plt.figure()
    cm = plt.cm.get_cmap('RdYlBu')
    z=np.arange(0,embedding.shape[0])/25
    plt.scatter(embedding[:, d1], embedding[:, d2],
                vmin=0, vmax=embedding.shape[0]/25, s=20, c=z, cmap=cm)

    plt.plot(embedding[:, d1], embedding[:, d2],linewidth=0.2,c='k')
    cbar = plt.colorbar()
    return embedding


def crop_frame(img,sz1=None,sz2=None):
    if len(img.shape)<3:
        img = np.expand_dims(img, axis=2)
    img = np.array(img)[..., ::-1]
    # img = brighter(img)
    img = histN(img)
    masked = img[:, np.any(img, axis=0)[:, 0]]
    masked = masked[np.any(img, axis=1)[:, 0], :]
    if sz1==None and sz2==None:
        reg = masked
    else:
        if sz2 == None:
            sz2=sz1
        x = (sz1 - masked.shape[0]) // 2
        y = (sz2 - masked.shape[1]) // 2
        reg = np.zeros((sz1,sz2,img.shape[2]),dtype=img.dtype)
        reg[x:(x + masked.shape[0]), y:(y + masked.shape[1]), :] = masked
    return np.squeeze(reg)


def explain(pca):
    percentage=pca.explained_variance_ratio_
    acc_pct=np.array(percentage)
    for i in range(len(percentage)):
        acc_pct[i]=sum(percentage[:i])*100
    plt.plot(acc_pct)
    plt.xlabel('dimemsionality')
    plt.ylabel('variance explained (%)')
    plt.ylim((0,100))
    print(acc_pct[-1])
    return acc_pct


def plot_3D(points,axlim=None,azim=None,elev=None,fig=None):
    from mpl_toolkits.mplot3d import Axes3D
    if fig == None:
        fig = plt.figure()
    # ax = fig.gca(projection='3d')
    ax = fig.add_subplot(111, projection='3d')

    ax.plot(points[:, 0], points[:, 1], points[:, 2], linewidth=1)
    z = np.arange(0, points.shape[0])
    cm = plt.cm.get_cmap('RdYlBu')
    ax.scatter(points[:, 0], points[:, 1], points[:, 2],c=z, s=1,cmap=cm)
    ax.set_xlabel('PC1')
    ax.set_ylabel('PC2')
    ax.set_zlabel('PC3')
    # if not axlim == None:
    ax.set_xlim3d(axlim[:, 0])
    ax.set_ylim3d(axlim[:, 1])
    ax.set_zlim3d(axlim[:, 2])
    if not azim == None:
        ax.view_init(elev, azim)
    return ax


def smooth(x, window_len=11, window='hanning',cycle = 0):
    nan_ind = np.isnan(x)
    for i in range(x.shape[0]):
        if np.isnan(x[i]):
            x[i] = x[i-1]
    if cycle:
        s = np.r_[x[-1:-window_len:-1], x, x[window_len - 1:0:-1]]

    else:
        s = np.r_[x[0:(window_len - 1):1], x, x[-window_len - 1:-2]]

    if window == 'flat':  # moving average
        w = np.ones(window_len, 'd')
    else:
        w = eval('np.' + window + '(window_len)')
    y = np.convolve(w / w.sum(), s, mode='valid')
    pad=(window_len-1)//2
    y = y[pad:-pad]
    y[nan_ind] = np.nan
    return y


def NDsmooth(x, window_len=11, window='hanning'):
    for n in range(x.shape[1]):
        x[:,n]=smooth(x[:,n],window_len, window)
    return x


def line_intersection(line1, line2):
    xdiff = (line1[0][0] - line1[1][0], line2[0][0] - line2[1][0])
    ydiff = (line1[0][1] - line1[1][1], line2[0][1] - line2[1][1])

    def det(a, b):
        return a[0] * b[1] - a[1] * b[0]

    div = det(xdiff, ydiff)
    if div == 0:
       raise Exception('lines do not intersect')

    d = (det(*line1), det(*line2))
    x = det(d, xdiff) / div
    y = det(d, ydiff) / div
    return x, y


def find_trough(vs,win = 25, pad = 35, plot = 1,low=0,p=0):
    # plt.figure()

    vs1 = vs.copy()
    if low:
        vs2=smooth(vs1,255,'flat',0)
        vs1 = vs1 -vs2 + vs2.mean()
        vs1=smooth(vs1,win,'flat',1)
        vs1 = smooth(vs1, 25, 'flat')

    vs1=smooth(vs1,win,'flat')
    from scipy import signal
    trough=signal.argrelextrema(vs1, np.less)[0]
    peak=signal.argrelextrema(vs1, np.greater)[0]
    peak=peak[np.r_[True,np.diff(peak)>25]]
    peak=peak[peak>pad]
    peak=peak[peak<(len(vs1)-pad)]
    n=0
    trough=[]
    for i in peak:
        trough.append(vs[n:i].argmin()+n)
        n = i
    trough.append(vs[n:].argmin() + n)
    # close = np.where(np.diff(trough)<pad)[0]
    # for i in close:
    #     if vs[trough[i]] > vs[trough[i+1]]:
    #         trough[i] = trough[i+1]
    #     else:
    #         trough[i+1] = trough[i]
    # trough = np.unique(trough)
    # trough=trough[vs1[trough]<vs1.mean()]
    # trough=trough[np.r_[True,np.diff(trough)>25]]
    trough = np.array(trough)
    trough=trough[trough>pad]
    trough=trough[trough<(len(vs1)-pad)]



    # trough=np.insert(trough,0,0)
    # trough=np.append(trough,len(vs3)-1)
    if plot == 1:
        plt.plot(vs)
        plt.plot(vs1,label='smoothed')
        plt.plot(trough, vs[trough], 'o')
        plt.plot(peak,vs1[peak],'o')
        plt.ylabel('speed')
        plt.xlabel('frame')
        print(str(len(trough)),'steps')
    if p:
        return trough,peak
    else:
        return trough


def find_cross(threshFocus,win=3):
    threshCrosses = np.nonzero(np.diff(threshFocus))
    onsetLogical = threshFocus[threshCrosses] == False
    offsetLogical = threshFocus[threshCrosses] == True
    tC = np.asarray(threshCrosses)
    onsets = tC[0, onsetLogical] + 1
    offsets = tC[0, offsetLogical]

    if len(onsets) == 0:
        onsets = np.array([0])

    if len(offsets) == 0:
        offsets = np.array([len(threshFocus)])

    if offsets[0] < onsets[0]:
        onsets = np.append([0], onsets)

    if offsets[len(offsets) - 1] < onsets[len(onsets) - 1]:
        offsets = np.append(offsets, [len(threshFocus) - 1])
    dur = offsets - onsets
    onsets = onsets[dur > win]
    offsets = offsets[dur > win]
    return onsets,offsets


def rereg(filename,flip=1):
    from tensorflow.keras.applications import vgg19
    model = vgg19.VGG19(weights='imagenet', include_top=False)
    model.compile(loss="categorical_crossentropy", optimizer="adam")

    cap = cv2.VideoCapture(filename)
    num_frames = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
    cap.set(cv2.CAP_PROP_POS_FRAMES, 2)
    succ, img = cap.read()
    # num_frames=200
    img0 = np.array(img)[..., ::-1]
    img0 = histN(img0)[:,:,0]
    if flip:
        mask = img0 > 0
        labels = skimage.measure.label(mask, connectivity=2)
        props = find_the_large_mask(labels, img0)
        img0 = center_rotated(img0, props, img0.shape[0])
        # plt.imshow(img0)
        # plt.show()


    vecRep=[]
    area = []
    # vecRep.append(fs.to_tex(img0, model, 224))
    reg=np.zeros(shape=(num_frames, img0.shape[0],img0.shape[1]),dtype=img0.dtype)
    # reg[0,...]=img0
    cap.set(cv2.CAP_PROP_POS_FRAMES, 0)

    for i in tqdm(range(0,num_frames)):
        succ, img = cap.read()
        if succ:
            img1 = np.array(img)[..., ::-1]
            img1 = histN(img1)[:,:,0]

            if flip:
                mask = img1 > 0
                labels = skimage.measure.label(mask, connectivity=2)
                props = find_the_large_mask(labels, img1)
                img1 = center_rotated(img1, props, img1.shape[0])
            # plt.imshow(img1,alpha=.5)


            try:
                img1_aligned, warp = rigid_reg(img1, img0,flip =flip)
                reg[i, ...] = img1_aligned
                # img0 = img1_aligned
                img1 = img1_aligned

            except:
                print('reg fail at frame',str(i))
            vecRep.append(to_tex(img1, model, 224))
            area.append(np.sum(img.sum(axis=2) > 0))

    vecRep=np.array(vecRep)
    joblib.dump((vecRep, area), filename[:-4] + '_vecRep_remap2')
    imageio.mimwrite(filename[:-4] + '-2.mp4', reg,fps=25)
    return reg


def d_F(F,dt):
    d_areas = []
    for i in range(F.shape[0] - dt):
        d_areas.append((F[i + dt] - F[i])/dt)
    d_areas = np.array(d_areas)
    return d_areas


def velocity(d_areas,win=25):
    v_areas = np.zeros(d_areas.shape[0])
    for i in range(d_areas.shape[0]):
        v_areas[i] = np.linalg.norm(d_areas[i, :])
    if win > 0:
        v_areas[v_areas==0]=np.nan
        v_areas = smooth(v_areas, win, 'flat')
    return v_areas


def projection(d,ds):
    proj = np.zeros(d.shape[0])
    orth = d.copy()
    for i in range(d.shape[0]):
        v1 = ds[i,:]
        v2 = d[i,:]
        projection = v1 * np.dot(v2,v1)/np.dot(v1,v1)
        proj[i] = np.dot(v2,v1)/np.linalg.norm(v1)
        orth[i,:] = v2 - projection
    return proj,orth


def colorline(
    x, y, z=None, cmap=plt.get_cmap('hot'), norm=plt.Normalize(0.0, 1.0),
        linewidth=3, alpha=1.0):
    """
    http://nbviewer.ipython.org/github/dpsanders/matplotlib-examples/blob/master/colorline.ipynb
    http://matplotlib.org/examples/pylab_examples/multicolored_line.html
    Plot a colored line with coordinates x and y
    Optionally specify colors in the array z
    Optionally specify a colormap, a norm function and a line width
    """

    # Default colors equally spaced on [0,1]:
    if z is None:
        z = np.linspace(0.0, 1.0, len(x))

    # Special case if a single number:
    if not hasattr(z, "__iter__"):  # to check for numerical input -- this is a hack
        z = np.array([z])

    z = np.asarray(z)

    segments = make_segments(x, y)
    lc = mcoll.LineCollection(segments, array=z, cmap=cmap, norm=norm,
                              linewidth=linewidth, alpha=alpha)

    ax = plt.gca()
    ax.add_collection(lc)

    return lc


def make_segments(x, y):
    """
    Create list of line segments from x and y coordinates, in the correct format
    for LineCollection: an array of the form numlines x (points per line) x 2 (x
    and y) array
    """

    points = np.array([x, y]).T.reshape(-1, 1, 2)
    segments = np.concatenate([points[:-1], points[1:]], axis=1)
    return segments


def plot_chunk_traj(traj,dur,s_win=25,speed=0,select=[],bg = 0, low = 0):
    plt.rc('font', family='Helvetica')
    fig = plt.figure(figsize=(8, 6))
    ax = fig.add_subplot(111, projection='3d')
    import seaborn as sns
    link_color_pal = sns.color_palette("husl",len(dur))
    # np.random.shuffle(link_color_pal)
    n=0
    if len(select)==0:
        select = np.arange(len(dur))
    for i in range(len(dur)):
        n1 = n + dur[i]
        text_points = traj[n:n1,:]
        n = n1
        # ax.plot(text_points[:, 0], text_points[:, 1], text_points[:, 2],
        #         linewidth=1,c=link_color_pal[i],alpha=.5)
        if len(dur) == 1:
            ax.plot(text_points[:, 0], text_points[:, 1], text_points[:, 2],
                    linewidth=1, c='k', alpha=.5)
        text_points2=text_points.copy()
        text_points3=text_points.copy()
        if s_win>1:
            text_smooth=NDsmooth(text_points2,s_win,'flat')
        else:
            text_smooth = text_points
        if speed:
            cm1 = plt.cm.get_cmap('hot')
            vs = velocity(d_F(text_points3, 1),55)
            vs1 = vs.copy()
            if low:
                vs2 = smooth(vs1, 255, 'flat', 0)
                vs1 = vs1 - vs2 + vs2.mean()
                vs = smooth(vs1, 25, 'flat')
            z1 = np.concatenate(([vs.mean()], vs), axis=0)
            s1=ax.scatter(text_smooth[:len(z1),0], text_smooth[:len(z1),1], text_smooth[:len(z1),2],
                            c=z1,s=3,vmin = z1.mean()-z1.std(), vmax = z1.mean()+z1.std()*2, cmap = cm1,alpha=.75)
            if len(dur)<3:
                ax.scatter(text_smooth[0, 0], text_smooth[0, 1], text_smooth[0, 2],
                            s=300, marker='o', c='g',alpha=0.5)
                ax.scatter(text_smooth[-1, 0], text_smooth[-1, 1], text_smooth[-1, 2],
                            s=300, marker='o', c='r',alpha=0.5)
                # ax.text(text_smooth[0, 0], text_smooth[0, 1], text_smooth[0, 2],
                #         fontsize=24, s='s', color='k', zorder=10)
                # ax.text(text_smooth[-1, 0], text_smooth[-1, 1],text_smooth[-1, 2],
                #         fontsize=24, s='e', color='k', zorder=10)
                ax.plot(text_smooth[:, 0], text_smooth[:, 1], text_smooth[:, 2],
                        linewidth=10, c=link_color_pal[i], alpha=.2,label=select[i])
        elif bg==0:
            ax.plot(text_smooth[:, 0], text_smooth[:, 1], text_smooth[:, 2],
                    linewidth=3, c=link_color_pal[i], alpha=1,label=select[i])
            ax.scatter(text_smooth[0, 0], text_smooth[0, 1], text_smooth[0, 2],
                        s=300, marker='o', c='g',alpha=0.5)
            ax.scatter(text_smooth[-1, 0], text_smooth[-1, 1], text_smooth[-1, 2],
                        s=300, marker='o', c='r',alpha=0.5)

    ax.set_xlabel('PC1',fontsize=24,labelpad=15)
    ax.set_ylabel('PC2',fontsize=24,labelpad=15)
    ax.set_zlabel('PC3',fontsize=24,labelpad=15)

    # if speed:
    #     cbar = fig.colorbar(s1, ax=ax, shrink=0.3, pad =0.05)
    #     # color_label = 'speed'
    #     color_label = 'velocity (a.u.)'
    #     cbar.ax.set_ylabel(color_label,fontsize=20)
    #     cbar.ax.tick_params(labelsize=18)
    # elif bg==0:
    #     ax.legend(loc='upper right', prop={'size': 16})
    plt.tick_params(labelsize=18)
    plt.locator_params(nbins=4)

    return ax



def same_scale(ax,axlim1,elev,azim):
    ax.set_xlim3d(axlim1[:, 0])
    ax.set_ylim3d(axlim1[:, 1])
    ax.set_zlim3d(axlim1[:, 2])
    ax.view_init(elev, azim)


def cuttlefish_mask(masterframe,gSigma3=50,factor = 1.75):
    gSigma1 = 1
    gSize1 = 15
    gSigma2 = 10
    gSize2 = 25
    # gSigma3 = 300
    gSize3 = int(2 * (2 * gSigma3) + 1)
    minSize = 100000
    blur1 = cv2.GaussianBlur(masterframe, (gSize1, gSize1), gSigma1)
    blur2 = cv2.GaussianBlur(masterframe, (gSize2, gSize2), gSigma2)
    blur3 = blur1 - blur2
    mask = np.zeros_like(blur3, dtype=bool)
    mask[blur3 > 0.5] = True
    blur3 = cv2.GaussianBlur(blur3 * mask, (gSize3, gSize3), gSigma3)
    mask = np.zeros_like(blur3, dtype=bool)
    mask[blur3 > blur3.std()*factor+blur3.mean()] = True
    # plt.imshow(masterframe)
    # plt.imshow(mask,alpha=0.5)

    cleaned = morphology.remove_small_objects(mask, minSize, 2)
    labels, num_labels = scipy.ndimage.label(cleaned)
    if num_labels == 0:
        return None
    else:
        cuttlefish_label = np.argmax(np.bincount(labels.flatten())[1:]) + 1
        return labels == cuttlefish_label


def plot_example(exp,video_basename,select,n_frame = 4):
    home = os.environ['HOME']
    indir = home + '/Dropbox/lab2/sepia/sepia213/'
    onsets, offsets = joblib.load( indir + video_basename + '.overlap')
    dur = offsets - onsets
    plt.figure(figsize=(n_frame*2,len(select)*2))
    chunk = joblib.load(home+'/Dropbox/lab2/sepia/' + exp + '/'+exp+'.textchunk')
    n=1
    for i in tqdm(select):
        chunk_id = np.where(onsets[i] >= chunk[0, :])[0][-1]
        on_ind = onsets[i] - chunk[0, chunk_id]
        off_ind = on_ind + offsets[i] - onsets[i]
        out_names = home+'/Dropbox/lab2/sepia/' + exp + '/' + 'overview' + str(chunk[0, chunk_id]) \
                    + '-' + str(chunk[1, chunk_id]) + '.mp4'
        cap = cv2.VideoCapture(out_names)
        step = dur[i]//(n_frame-1)

        for k in range(n_frame):
            frame = on_ind+step*k
            cap.set(cv2.CAP_PROP_POS_FRAMES, frame)
            succ, img = cap.read()
            if not succ:
                frame = off_ind-1
                cap.set(cv2.CAP_PROP_POS_FRAMES, frame)
                succ, img = cap.read()
            nplus=0
            while img.sum() == 0:
                succ, img = cap.read()
                nplus += 1
            img = histN(img[:, :, ::-1])
            pad = img.shape[0]//5
            img = img[pad:(-pad),pad:(-pad),:]
            plt.subplot(len(select),n_frame,n)
            plt.imshow(img)
            plt.title(frame+chunk[0, chunk_id]+nplus)
            plt.axis('off')
            if k == 0:
                plt.text(-img.shape[0]//5,img.shape[0]//2,str(i),fontsize=24)
            n = n+1
    plt.tight_layout()
    if len(select) == len(dur):
        plt.savefig(indir+'/plot/'+video_basename + '_example.png', dpi=300, transparent=True)
    else:
        plt.savefig(indir+'/plot/'+video_basename + str(select)+'_example.png', dpi=300, transparent=True)

    plt.close('all')


def merge_trough(trough,areas_s,speed2,thresh=0):
    if thresh == 0:
        trough_jump = velocity(d_F(areas_s[trough, :3], 1), 0)
        thresh = trough_jump.mean()
    k0 = len(trough)
    k1 = 0
    while k0 > k1:
        k0 = len(trough)
        trough_jump = velocity(d_F(areas_s[trough, :3], 1), 0)
        close = np.where(trough_jump < thresh)[0]
        for i in close:
            if speed2[trough[i]] > speed2[trough[i + 1]]:
                trough[i] = trough[i + 1]
            else:
                trough[i + 1] = trough[i]
        trough = np.unique(trough)
        k1 = len(trough)
        print(len(trough), 'steps')
    return trough


def remove_250(areas_sub1):
    speed = velocity(d_F(areas_sub1, 1), 0)
    # plt.plot(speed)
    peak = np.where(speed > (speed.mean() + speed.std() * 5))[0]
    # p1 = peak[np.where(np.diff(peak) == 250)[0]][0]
    p250 = np.where(np.diff(peak) == 250)[0]
    if len(p250)>0:
        p1 = peak[p250][0]
    else:
        p1 = peak[0]

    p0 = p1 % 250
    for k in range((areas_sub1.shape[0] - p0) // 250 + 1):
        pk = p0 + 250 * k
        if (pk + 20) < areas_sub1.shape[0] :
            for j in range(9):
                areas_sub1[pk + j, :] = areas_sub1[pk + j + 10, :]
    return areas_sub1,p0


def binning(speed_Ys,sample = 500,plot=1):
    step = (speed_Ys[0, :].max() - speed_Ys[0, :].min()) / sample
    width = step*10
    summary = np.zeros((sample,4))
    for i in range(sample):
        band = speed_Ys[0, :].min()+step*i
        p = np.where((speed_Ys[0, :] < band + width)*(speed_Ys[0, :]>band-width))[0]
        summary[i, 0] = band
        summary[i, 1] = np.nanmean((speed_Ys[1, p]))
        summary[i, 2] = np.nanstd((speed_Ys[1, p]))
        summary[i, 3] = len(speed_Ys[1, p])
    sem = summary[:,2] / np.sqrt(summary[:,3])
    if plot:
        plt.figure()
        ax1 = plt.subplot()
        plt.plot(summary[:, 0],summary[:, 1], linewidth=1)
        ax1.fill_between(summary[:, 0], summary[:, 1] - sem, summary[:, 1] + sem,
                          alpha=0.5)
        ax1.set_ylabel('speed (a.u.)',fontsize=18)
        plt.tick_params(labelsize=12)
        plt.locator_params(nbins=5)
    return summary


def LLE_plot(Y2, X2,dur1,select1,plot=1):
    n = 0
    speed_Ys = []
    for i in range(len(dur1)):
        n1 = n + dur1[i]
        Y_s = Y2[n:n1, :]
        dt = 1
        points_sub1 = X2[n:n1, :].copy()
        speed = velocity(d_F(points_sub1, dt), 55)
        # speed = velocity(d_F(NDsmooth(points_sub1,55), dt), 25)
        n = n1
        if plot:
            plt.plot(Y_s[:, 0],label=select1[i])
        speed_Ys.append(np.vstack((Y_s[dt:, 0], speed)))
    speed_Ys = np.hstack(speed_Ys)
    # plt.legend()
    if plot:
        plt.xlabel('frame')
        plt.ylabel('LLE')
    return speed_Ys

def LLE_plot2(Y1, dur1, X2,dur2,select1,plot=1):
    n = 0
    k = 0
    speed_Ys = []
    for i in range(len(dur1)):
        n1 = n + dur1[i]
        k1 = k + dur2[i]
        Y_s = Y1[n:n1, :]
        dt = 1
        points_sub1 = X2[k:k1, :].copy()
        filter1 = np.isnan(points_sub1.mean(axis=1))
        speed = velocity(d_F(points_sub1, dt), 55)
        # filter1 = np.isnan(speed)
        speed = speed[~filter1[dt:]]
        speed = nan_fill(speed)
        # filter2 = np.isnan(speed)
        # speed = velocity(d_F(NDsmooth(points_sub1,55), dt), 25)
        n = n1
        k = k1
        if plot:
            plt.plot(Y_s[:, 0],label=select1[i])
        speed_Ys.append(np.vstack((Y_s[dt:, 0], speed)))
    speed_Ys = np.hstack(speed_Ys)
    # plt.legend()
    if plot:
        plt.xlabel('frame')
        plt.ylabel('LLE')
    return speed_Ys

def nan_fill(arr):
    mask = np.isnan(arr)
    idx = np.where(mask)[0]
    arr[idx] = arr[idx+1]
    return arr

def binning_hist(Y,points_sub2,dur2,summary,bin=75,shuffle=0):
    points_all1 = points_sub2
    trough_list = []
    trough_ind = []
    n = 0
    for i in range(len(dur2)):
        n1 = n + dur2[i]
        Y_s = Y[n:n1, :]
        dt = 1
        points_sub1 = points_all1[n:n1, :]
        speed = velocity(d_F(points_sub1, dt), 55)
        win = speed.shape[0] // 40
        if win % 2 == 0:
            win = win + 1
        # speed = fs.smooth(speed,25)
        trough = find_trough(speed, win, 25, plot = 1)
        trough = merge_trough(trough,Y_s,speed,
                              (summary[:,0].max()-summary[:,0].min())/bin)
        trough_list = np.hstack((trough_list, Y_s[dt:, 0][trough]))
        trough_ind = np.hstack((trough_ind, trough+n))

        n = n1

    if shuffle>0:
        h = []
        summary_list = []
        summary_list2 = []

        for k in tqdm(range(shuffle)):
            trough_list2=[]
            n = 0
            speed_Ys = []
            for i in range(len(dur2)):
                n1 = n + dur2[i]
                Y_s = Y[n:n1,0].copy()
                dt = 1
                points_sub1 = points_all1[n:n1, :]
                points_all1_shuffle = points_sub1.copy()
                shuffle_n = np.arange(dur2[i])
                np.random.shuffle(shuffle_n)
                points_all1_shuffle = points_all1_shuffle[shuffle_n,:]
                Y_s = Y_s[shuffle_n]

                speed = velocity(d_F(points_all1_shuffle, dt), 55)
                win=speed.shape[0]//40
                if win%2 == 0:
                    win = win + 1
                # speed = smooth(speed,55)
                speed = smooth(speed,25)
                trough = find_trough(speed, win, 25, plot = 0)
                trough_list2 = np.hstack((trough_list2,Y_s[dt:][trough]))
                speed_Ys.append(np.vstack((Y_s[dt:], speed)))
                n = n1
            counts, bins = np.histogram(trough_list2,bin)
            # plt.hist(bins[:-1], bins, weights=counts)
            h.append(counts)
            speed_Ys = np.hstack(speed_Ys)
            summary2 = binning(speed_Ys, 750,0)
            summary_list.append(speed_Ys)
            summary_list2.append(summary2)


        summary_list = np.array(summary_list)
        summary_list2 = np.array(summary_list2)


        h = np.array(h)
        counts0, bins = np.histogram(trough_list, bin)
        ab_counts = counts0 - (h.mean(axis=0)+h.std(axis=0)/np.sqrt(shuffle))
        ab_counts[ab_counts < 0] = 0
        ab_counts[ab_counts > 0] = ab_counts[ab_counts >0] + (h.mean(axis=0))[ab_counts >0]

    fig, ax1 = plt.subplots()
    if shuffle > 0:
        ax1.hist(bins[:-1], bins, weights=h.mean(axis=0),color='red',alpha=0.5)
        h0 = ax1.hist(bins[:-1], bins, weights=ab_counts, color='tab:blue')
        # ax1.plot(bins[:-1],h.mean(axis=0),color='red')
        # # ax1.fill_between(bins[:-1],h.mean(axis=0) - h.std(axis=0)/np.sqrt(shuffle),
        # #                  h.mean(axis=0) + h.std(axis=0)/np.sqrt(shuffle),
        # #                  color='r', alpha=0.2)
        # ax1.fill_between(bins[:-1],np.repeat(0,len(bins)-1),
        #                  h.mean(axis=0) + h.std(axis=0)/np.sqrt(shuffle),
        #                  color='r', alpha=0.2)
        # h0 = ax1.hist(trough_list, bin, color='tab:blue')
    else:
        h0 = ax1.hist(trough_list, bin, color='tab:blue')
    ax1.set_ylabel('trough#', color='tab:blue')
    ax1.tick_params(axis='y', labelcolor='tab:blue')
    ax1.set_xlabel('LLE')

    ax2 = ax1.twinx()
    ax2.set_ylabel('speed', color='black')
    ax2.plot(summary[:, 0], summary[:, 1], linewidth=1, color='black')
    sem = summary[:, 2] / np.sqrt(summary[:, 3])
    ax2.fill_between(summary[:, 0], summary[:, 1] - sem, summary[:, 1] + sem,
                     color='gray', alpha=0.5)


    # if shuffle > 0:
    #     summary2 = summary_list2.mean(axis = 0)
    #     normalized_speed = summary2[:, 1] * (summary[:, 1].mean()/ summary2[:, 1].mean())
    #     ax2.plot(summary2[:, 0], normalized_speed,linewidth=1, color='r')
    #     sem = summary2[:, 2] / np.sqrt(summary2[:, 3]) * (summary[:, 2].mean()/ summary2[:, 2].mean())
    #     ax2.fill_between(summary2[:, 0], normalized_speed - sem, normalized_speed + sem,
    #                      color='r', alpha=0.2)

    plt.tight_layout()
    trough_info = np.vstack((trough_ind,trough_list))
    if shuffle > 0:
        return h0, trough_info,summary_list
    else:
        return h0, trough_info


def binning_hist2(Y,dur1,points_sub2,dur2,summary,bin=75,shuffle=0):
    points_all1 = points_sub2
    trough_list = []
    trough_ind = []
    n = 0
    k = 0
    for i in range(len(dur2)):
        n1 = n + dur1[i]
        k1 = k + dur2[i]

        Y_s = Y[n:n1, :]
        dt = 1
        points_sub1 = points_all1[k:k1, :]
        speed = velocity(d_F(points_sub1, dt), 55)
        if points_sub2.shape[0] > Y.shape[0]:
            filter1 = np.isnan(points_sub1.mean(axis=1))
            speed = velocity(d_F(points_sub1, dt), 55)
            speed = speed[~filter1[dt:]]
            speed = nan_fill(speed)
        win = speed.shape[0] // 40
        if win % 2 == 0:
            win = win + 1
        # speed = fs.smooth(speed,25)
        trough = find_trough(speed, win, 25, plot = 1)
        trough = merge_trough(trough,Y_s,speed,
                              (summary[:,0].max()-summary[:,0].min())/bin)
        trough_list = np.hstack((trough_list, Y_s[dt:, 0][trough]))
        trough_ind = np.hstack((trough_ind, trough+n))

        n = n1
        k = k1

    if shuffle>0:
        h = []
        summary_list = []
        summary_list2 = []
        for j in tqdm(range(shuffle)):
            trough_list2=[]
            n = 0
            k = 0
            speed_Ys = []
            for i in range(len(dur2)):
                n1 = n + dur1[i]
                k1 = k + dur2[i]

                Y_s = Y[n:n1,0].copy()
                dt = 1
                points_sub1 = points_all1[k:k1, :]
                if points_sub2.shape[0] > Y.shape[0]:
                    filter1 = np.isnan(points_sub1.mean(axis=1))
                    points_sub1 = points_sub1[~filter1,:]
                points_all1_shuffle = points_sub1.copy()
                shuffle_n = np.arange(dur1[i])
                np.random.shuffle(shuffle_n)
                points_all1_shuffle = points_all1_shuffle[shuffle_n,:]
                Y_s = Y_s[shuffle_n]

                speed = velocity(d_F(points_all1_shuffle, dt), 55)
                win=speed.shape[0]//40
                if win%2 == 0:
                    win = win + 1
                # speed = smooth(speed,55)
                speed = smooth(speed,25)
                trough = find_trough(speed, win, 25, plot = 0)
                trough_list2 = np.hstack((trough_list2,Y_s[dt:][trough]))
                speed_Ys.append(np.vstack((Y_s[dt:], speed)))
                n = n1
                k = k1

            counts, bins = np.histogram(trough_list2,bin)
            # plt.hist(bins[:-1], bins, weights=counts)
            h.append(counts)
            speed_Ys = np.hstack(speed_Ys)
            summary2 = binning(speed_Ys, 750,0)
            summary_list.append(speed_Ys)
            summary_list2.append(summary2)


        summary_list = np.array(summary_list)
        summary_list2 = np.array(summary_list2)


        h = np.array(h)
        counts0, bins = np.histogram(trough_list, bin)
        ab_counts = counts0 - (h.mean(axis=0)+h.std(axis=0)/np.sqrt(shuffle))
        ab_counts[ab_counts < 0] = 0
        ab_counts[ab_counts > 0] = ab_counts[ab_counts >0] + (h.mean(axis=0))[ab_counts >0]

    fig, ax1 = plt.subplots()
    if shuffle > 0:
        ax1.hist(bins[:-1], bins, weights=h.mean(axis=0),color='red',alpha=0.5)
        h0 = ax1.hist(bins[:-1], bins, weights=ab_counts, color='tab:blue')
        # ax1.plot(bins[:-1],h.mean(axis=0),color='red')
        # # ax1.fill_between(bins[:-1],h.mean(axis=0) - h.std(axis=0)/np.sqrt(shuffle),
        # #                  h.mean(axis=0) + h.std(axis=0)/np.sqrt(shuffle),
        # #                  color='r', alpha=0.2)
        # ax1.fill_between(bins[:-1],np.repeat(0,len(bins)-1),
        #                  h.mean(axis=0) + h.std(axis=0)/np.sqrt(shuffle),
        #                  color='r', alpha=0.2)
        # h0 = ax1.hist(trough_list, bin, color='tab:blue')
    else:
        h0 = ax1.hist(trough_list, bin, color='tab:blue')
    ax1.set_ylabel('trough#', color='tab:blue')
    ax1.tick_params(axis='y', labelcolor='tab:blue')
    ax1.set_xlabel('LLE')

    ax2 = ax1.twinx()
    ax2.set_ylabel('speed', color='black')
    ax2.plot(summary[:, 0], summary[:, 1], linewidth=1, color='black')
    sem = summary[:, 2] / np.sqrt(summary[:, 3])
    ax2.fill_between(summary[:, 0], summary[:, 1] - sem, summary[:, 1] + sem,
                     color='gray', alpha=0.5)


    # if shuffle > 0:
    #     summary2 = summary_list2.mean(axis = 0)
    #     normalized_speed = summary2[:, 1] * (summary[:, 1].mean()/ summary2[:, 1].mean())
    #     ax2.plot(summary2[:, 0], normalized_speed,linewidth=1, color='r')
    #     sem = summary2[:, 2] / np.sqrt(summary2[:, 3]) * (summary[:, 2].mean()/ summary2[:, 2].mean())
    #     ax2.fill_between(summary2[:, 0], normalized_speed - sem, normalized_speed + sem,
    #                      color='r', alpha=0.2)

    plt.tight_layout()
    trough_info = np.vstack((trough_ind,trough_list))
    if shuffle > 0:
        return h0, trough_info,summary_list
    else:
        return h0, trough_info




def subsample(X2,sample,d = 0):
    step = (X2[:, d].max() - X2[:, d].min()) / sample
    width = step * 2
    subsmaple = []
    for i in range(sample):
        band = X2[:, d].min() + step * i
        p = np.where((X2[:, d] < band + width) * (X2[:, d] > band - width))[0]
        if len(p)>0:
            index = np.random.choice(p.shape[0], 1, replace=False)
            subsmaple.append(p[index])

    subsmaple.sort()
    X3 = X2[subsmaple, :].squeeze()
    return X3


def select_taj(select1,points_all,chunk_ind):
    points_sub = []
    dur1 = []
    for i in select1:
        point_ind = np.where(chunk_ind == i)[0]
        points_sub1 = points_all[point_ind,:]
        points_sub2,p0 = remove_250(points_sub1)
        for k in range((points_sub1.shape[0] - p0) // 250 + 1):
            pk = p0 + 250 * k
            if (pk + 20) < points_sub1.shape[0] :
                for j in range(9):
                    points_sub1[pk + j, :] = np.nan
        filter1 = np.isnan(points_sub1.mean(axis=1))
        points_sub1 = points_sub1[~filter1, :]

        points_sub.append(points_sub1)
        dur1.append(points_sub1.shape[0])
    points_sub = np.vstack((points_sub))
    dur1 = np.array(dur1)
    return points_sub,dur1


def select_taj_speed(select1,points_all,chunk_ind,norm=True):
    points_sub = []
    speed_sub = []
    dur1 = []
    for i in select1:
        point_ind = np.where(chunk_ind == i)[0]
        points_sub1 = points_all[point_ind,:]
        points_sub2,p0 = remove_250(points_sub1)
        for k in range((points_sub1.shape[0] - p0) // 250 + 1):
            pk = p0 + 250 * k
            if (pk + 20) < points_sub1.shape[0] :
                for j in range(9):
                    points_sub1[pk + j, :] = np.nan

        filter1 = np.isnan(points_sub1.mean(axis=1))
        # filter1_id = np.where(filter1)[0]
        # filter1_id = np.append(filter1_id, filter1_id[np.where(np.diff(filter1_id) > 2)[0]] + 1)
        # filter1_id = np.append(filter1_id, filter1_id[np.where(np.diff(filter1_id) > 2)[0]] + 1)
        # filter1_id.sort()
        speed = velocity(d_F(points_sub1, 1), 0)
        filter2 = np.isnan(speed)
        speed = speed[~filter2]
        # speed = np.delete(speed, filter1_id - 2)
        points_sub1 = points_sub1[~filter1,:]
        speed = smooth(speed, 55, 'flat')
        # h = np.histogram(speed, 100)
        # h[1][h[0].argmax()]

        if norm:
            h = np.histogram(speed, 100)
            speed_d = h[1][h[0].argmax()]
            speed = speed - speed_d
            speed = speed - speed.min()
        # speed = (speed - speed.mean())/speed.std()

        # speed = d_F(speed,25)
        points_sub.append(points_sub1)
        speed_sub = np.hstack((speed_sub, speed))
        dur1.append(points_sub1.shape[0])
    points_sub = np.vstack((points_sub))
    dur1 = np.array(dur1)
    return points_sub,speed_sub,dur1




def select_taj2(select1, areas_f,points_all,mask_ind2):
    areas = areas_f['areas']
    chunk_ind = np.array(areas_f['chunk_indexes'])
    frame_ind = np.array(areas_f['frame_indexes'])
    frame_sub = []
    areas_sub = []
    points_sub = []
    dur1 = []
    for i in tqdm(select1):
        point_ind = np.where(chunk_ind == i)[0]
        frame_ind1 = frame_ind[point_ind]

        points_sub1 = points_all[point_ind, :]
        points_sub2,p0 = remove_250(points_sub1)
        areas_sub1 = areas[point_ind, :]
        areas_sub1 = areas_sub1[:,mask_ind2].astype(float)
        for k in range((areas_sub1.shape[0] - p0) // 250 + 1):
            pk = p0 + 250 * k
            if (pk + 20) < areas_sub1.shape[0] :
                for j in range(9):
                    areas_sub1[pk + j, :] = np.nan
        filter1 = areas_sub1.mean(axis=1) > 0


        points_sub.append(points_sub2[filter1,:])
        areas_sub.append(areas_sub1[filter1,:])

        frame_sub.append(frame_ind1[filter1])
        dur1.append(points_sub2[filter1,:].shape[0])
    frame_sub= np.hstack((frame_sub))
    points_sub = np.vstack((points_sub))
    areas_sub = np.vstack((areas_sub))
    dur1 = np.array(dur1)
    return points_sub,areas_sub,frame_sub,dur1



def select_taj3(select1, areas_f,points_all,mask_ind2):
    areas = areas_f['areas']
    chunk_ind = np.array(areas_f['chunk_indexes'])
    frame_ind = np.array(areas_f['frame_indexes'])
    frame_sub = []
    areas_sub = []
    points_sub = []
    dur1 = []
    for i in tqdm(select1):
        point_ind = np.where(chunk_ind == i)[0]
        frame_ind1 = frame_ind[point_ind]

        points_sub1 = points_all[point_ind, :]
        points_sub2,p0 = remove_250(points_sub1)
        areas_sub1 = areas[point_ind, :]
        areas_sub1 = areas_sub1[:,mask_ind2].astype(float)
        for k in range((areas_sub1.shape[0] - p0) // 250 + 1):
            pk = p0 + 250 * k
            if (pk + 20) < areas_sub1.shape[0] :
                for j in range(9):
                    areas_sub1[pk + j, :] = np.nan
                    points_sub1[pk + j, :] = np.nan
        # filter1 = areas_sub1.mean(axis=1) > 0
        points_sub.append(points_sub2)
        areas_sub.append(areas_sub1)
        frame_sub.append(frame_ind1)
        dur1.append(points_sub2.shape[0])
    frame_sub= np.hstack((frame_sub))
    points_sub = np.vstack((points_sub))
    areas_sub = np.vstack((areas_sub))
    dur1 = np.array(dur1)
    return points_sub,areas_sub,frame_sub,dur1


def taj_decomp(select1,points_all,chunk_ind,areas,mask_ind2,labels_chr):
    std_sub = []
    mean_sub = []
    areas_sub = []
    dur1 = []
    for i in select1:
        point_ind = np.where(chunk_ind == i)[0]
        points_sub1 = points_all[point_ind, :]
        points_sub2, p0 = remove_250(points_sub1)
        areas_sub1 = areas[point_ind, :]
        areas_sub1 = areas_sub1[:, mask_ind2].astype(float)
        for k in range((areas_sub1.shape[0] - p0) // 250 + 1):
            pk = p0 + 250 * k
            if (pk + 20) < areas_sub1.shape[0]:
                for j in range(9):
                    areas_sub1[pk + j, :] = np.nan
        areas_mean_list = []
        areas_std_list = []
        for comp_id in tqdm(np.unique(labels_chr)):
            chrom_ind = np.where(labels_chr == comp_id)[0]
            areas_cluster = areas_sub1[:, chrom_ind]
            if comp_id == 0:
                areas_mean = areas_cluster.mean(axis=1)
                filter1 = areas_mean > 0
            areas_cluster = areas_cluster[filter1]
            areas_mean = areas_cluster.mean(axis=1)
            areas_std = areas_cluster.std(axis=1)
            areas_mean_list.append(areas_mean)
            areas_std_list.append(areas_std)
        areas_mean_list = np.array(areas_mean_list).T
        areas_std_list = np.array(areas_std_list).T
        std_sub.append(areas_std_list)
        mean_sub.append(areas_mean_list)
        dur1.append(areas_std_list.shape[0])
        areas_sub.append(areas_sub1[filter1, :])

    std_sub = np.vstack((std_sub))
    mean_sub = np.vstack((mean_sub))
    areas_sub = np.vstack((areas_sub))

    dur1 = np.array(dur1)
    return mean_sub,std_sub,areas_sub,dur1

def taj_decomp_sub(areas_sub1,labels_chr):
    std_sub = []
    mean_sub = []
    dur1 = []
    areas_mean_list = []
    areas_std_list = []
    for comp_id in tqdm(np.unique(labels_chr)):
        chrom_ind = np.where(labels_chr == comp_id)[0]
        areas_cluster = areas_sub1[:, chrom_ind]
        areas_mean = areas_cluster.mean(axis=1)
        areas_std = areas_cluster.std(axis=1)
        areas_mean_list.append(areas_mean)
        areas_std_list.append(areas_std)
    areas_mean_list = np.array(areas_mean_list).T
    areas_std_list = np.array(areas_std_list).T
    std_sub.append(areas_std_list)
    mean_sub.append(areas_mean_list)
    dur1.append(areas_std_list.shape[0])
    std_sub = np.vstack((std_sub))
    mean_sub = np.vstack((mean_sub))
    return mean_sub,std_sub



def draw_frame(trough2,select1,video_name,center,prop):
    areas_f = h5py.File( video_name + '.areas', 'r')
    areas = areas_f['areas']
    chunk_ind = np.array(areas_f['chunk_indexes'])
    cq_f = h5py.File(video_name + '.cleanqueen', 'r')
    cq = cq_f['Chromatophore']

    point_ind = np.where(chunk_ind == select1[0])[0]
    img_list = []
    for n in tqdm(trough2):
        frame = int(point_ind[0] + n)
        if frame > point_ind[-1]:
            frame = point_ind[-1]
        z_all3 = areas[frame, :]
        r = np.sqrt(z_all3 / np.pi) #* 1.25
        fake_seg = np.zeros(cq.shape, dtype=int)
        mask_ind3 = np.where((r > 0) * (r < 10))[0]
        for p in mask_ind3:
            rr, cc = skimage.draw.circle(center[1, p - 1], center[0, p - 1],
                                         r[p], fake_seg.shape)
            fake_seg[rr, cc] = 255
        crop = fake_seg[prop._slice]
        M = cv2.getRotationMatrix2D((crop.shape[1] / 2, crop.shape[0] / 2), -prop.orientation / np.pi * 180, 1)
        crop_r = cv2.warpAffine(crop.astype(float), M, (crop.shape[1], crop.shape[0]))
        reverse_img_small = cv2.resize(crop_r.astype(float), dsize=(crop_r.shape[1] // 8, crop_r.shape[0] // 8),
                                       interpolation=cv2.INTER_AREA)
        img_list.append(reverse_img_small)
    return img_list


def draw_frame2(frame,areas_sub,center,prop,mask_ind2,cq,scale = 8):
    img_list = []
    for n in tqdm(frame):
        z_all3 = areas_sub[n, :]
        r = np.sqrt(z_all3 / np.pi)
        # r = (z_all3 / np.pi)
        fake_seg = np.zeros(cq.shape, dtype=int)
        mask_ind3 = np.where((r > 1) * (r < 5))[0]
        for p in mask_ind3:
            rr, cc = skimage.draw.circle(center[1, mask_ind2[p]-1], center[0, mask_ind2[p]-1],
                                         r[p], fake_seg.shape)
            fake_seg[rr, cc] = 255
        crop = fake_seg[prop._slice]
        M = cv2.getRotationMatrix2D((crop.shape[1] / 2, crop.shape[0] / 2), -prop.orientation / np.pi * 180, 1)
        crop_r = cv2.warpAffine(crop.astype(float), M, (crop.shape[1], crop.shape[0]))
        mask_r = cv2.warpAffine(prop.filled_image.astype(float), M, (crop.shape[1], crop.shape[0]))
        mask_r_small = cv2.resize(mask_r.astype(float), dsize=(crop_r.shape[1] // scale, crop_r.shape[0] // scale),
                                       interpolation=cv2.INTER_AREA)
        reverse_img_small = cv2.resize(crop_r.astype(float), dsize=(crop_r.shape[1] // scale, crop_r.shape[0] // scale),
                                       interpolation=cv2.INTER_AREA)
        reverse_img_small[mask_r_small == 0] = np.nan
        img_list.append(reverse_img_small)
    return img_list



from matplotlib import transforms


def plot_CP(pca3,mask_ind2,d,indir):
    # center, prop = joblib.load(indir + 'draw_parameter')
    (center,labels,mask) = joblib.load(indir+'draw_parameter2')
    prop = find_the_large_mask(labels.T, mask.T)

    anlge = -prop.orientation / np.pi * 180
    rot = transforms.Affine2D().rotate_deg(anlge - 90)
    plt.figure()
    ax1 = plt.subplot()
    base = ax1.transData
    CP = pca3.components_
    cm = plt.cm.get_cmap('bwr')
    temp_f = CP[d, :]
    plt.scatter(center[1, mask_ind2 - 1], center[0, mask_ind2 - 1], cmap=cm, c=temp_f,transform= rot + base,
                s=.05, alpha = 0.8, vmin=temp_f.mean() - temp_f.std(), vmax=temp_f.mean() + temp_f.std())
    plt.text(10, 5500, 'PC' + str(d + 1), color='w', fontsize=24)
    ax1.axis('off')
    ax1.axis('equal')

from scipy.optimize import curve_fit
def sigmoid(x, L ,x0, k, b):
    y = L / (1 + np.exp(-k*(x-x0)))+b
    return (y)

# def sig_p(ydata):
#     xdata = np.arange(0,len(ydata))
#     p0 = [max(ydata), np.median(xdata),1,min(ydata)] # this is an mandatory initial guess
#     popt, pcov = curve_fit(sigmoid, xdata, ydata,p0, method='dogbox')
#     return (popt[1])

def sig_p(areas_cluster_s2):
    p = np.zeros((areas_cluster_s2.shape[1]))
    for i in tqdm(range(areas_cluster_s2.shape[1])):
        ydata = areas_cluster_s2[:,i]
        xdata = np.arange(0,len(ydata))
        p0 = [max(ydata), np.median(xdata),1,min(ydata)] # this is an mandatory initial guess
        try:
            popt, pcov = curve_fit(sigmoid, xdata, ydata,p0, method='dogbox')
            if popt[1]>0:
                p[i] = popt[1]
        except:
            pass
    return (p)

def acf(x):
    n = len(x)
    variance = x.var()
    x = x-x.mean()
    r = np.correlate(x, x, mode = 'full')[-n:]
    #assert N.allclose(r, N.array([(x[:n-k]*x[-(n-k):]).sum() for k in range(n)]))
    result = r/(variance*(np.arange(n, 0, -1)))
    return result


def prePCA(areas_sub,min=0):
    if min == 1:
        areas_sub_n = (areas_sub - areas_sub.min(axis=0)) / (areas_sub.max(axis=0) - areas_sub.min(axis=0))
    elif min == 2:
        areas_sub_n = (areas_sub - areas_sub.min(axis=0)) /areas_sub.std(axis=0)
    else:
        areas_sub_n = (areas_sub - areas_sub.mean(axis=0)) / areas_sub.std(axis=0)
    areas_sub_n[:,areas_sub.std(axis=0)==0] = 0
    return areas_sub_n


def comp_heat(areas_sub,select_id1,labels_chr,mask_ind2,masterframe,indir):
    color2 = plt.cm.hsv(np.linspace(0, 1, len(select_id1) + 4))
    left, right,up, dwon = joblib.load(indir + 'mask_range')
    # center, prop = joblib.load(indir + 'draw_parameter')
    (center,labels,mask) = joblib.load(indir+'draw_parameter2')
    prop = find_the_large_mask(labels.T, mask.T)

    anlge = -prop.orientation / np.pi * 180
    rot = transforms.Affine2D().rotate_deg(anlge - 90)
    n = 1
    n1 = int(np.sqrt(len(select_id1)))
    n2 = int(np.ceil((len(select_id1)) / n1))
    fig = plt.figure(figsize=(n2 * 6, n1 * 4))
    for cluster_id in select_id1:
        ax1 = plt.subplot(n1, n2 * 2, n)
        n = n + 1

        if n == 2:
            ax2 = plt.subplot(n1, n2 * 2, n)
        else:
            ax3 = plt.subplot(n1, n2 * 2, n, sharex=ax2)

        chrom_ind = np.where(labels_chr == cluster_id)[0]
        areas_cluster = areas_sub[:, chrom_ind]
        plt.imshow(prePCA(areas_cluster[:, :]).T, cmap='hot', vmin=0, vmax=5, aspect='auto')

        base = ax1.transData
        ax1.imshow(255 - masterframe, transform=rot + base, cmap='gray', alpha=.5, origin="lower")
        cc = center[:, mask_ind2[np.where(labels_chr == cluster_id)[0]] - 1]
        color = color2[np.where(select_id1 == cluster_id)[0]][0]
        sca1 = ax1.scatter(cc[1, :], cc[0, :], s=.03, transform=rot + base, color=color)
        ax1.axis('off')
        ax1.set_xlim((left, right))
        ax1.set_ylim((up, dwon))

        n = n + 1
    plt.tight_layout()


def plot_chrom_map(chrom_ind,ax1,masterframe,labels_chr1,mask_ind2,indir):
    left, right,up, dwon = joblib.load(indir + 'mask_range')
    # center, prop = joblib.load(indir + 'draw_parameter')
    (center,labels,mask) = joblib.load(indir+'draw_parameter2')
    prop = find_the_large_mask(labels.T, mask.T)

    anlge = -prop.orientation / np.pi * 180
    rot = transforms.Affine2D().rotate_deg(anlge - 90)
    color1 = plt.cm.hsv(np.linspace(0, 1, len(np.unique(labels_chr1))))

    base = ax1.transData
    ax1.imshow(255 - masterframe, transform=rot + base, cmap='gray', alpha=.5, origin="lower")
    clu_sub = labels_chr1[chrom_ind]
    ind_sub = mask_ind2[chrom_ind] - 1
    for clu in np.unique(clu_sub):
        cc = center[:, ind_sub[np.where(clu_sub == clu)[0]]]
        color = color1[np.where(np.unique(labels_chr1) == clu)[0]][0]
        sca1 = ax1.scatter(cc[1, :], cc[0, :], s=.03, transform=rot + base, color=color)

    ax1.axis('off')
    ax1.set_xlim((left, right))
    ax1.set_ylim((up, dwon))


def plt_chrom_heat(areas_clusters,rank_cluster,ax4,chrom_ind,labels_chr2):
    color2 = plt.cm.hsv(np.linspace(0, 1, len(np.unique(labels_chr2))))
    ax4.imshow((areas_clusters).T, cmap='hot', vmin=0, vmax=5, aspect='auto',
                extent = [0, areas_clusters.shape[0] / 25, areas_clusters.shape[1],0])
    # ax4.imshow((areas_clusters).T, cmap='hot', vmin=0, vmax=5, aspect='auto')

    clu_sub = labels_chr2[chrom_ind]
    for clu in np.unique(clu_sub):
        reo_clu = np.where(clu_sub[rank_cluster] == clu)[0]
        color = color2[np.where(np.unique(labels_chr2) == clu)[0]][0]
        for i in reo_clu:
            ax4.vlines(areas_clusters.shape[0]/25 + 5, ymin=i, ymax=i+1, linewidth=15, color=color)
    ax4.set_xlim((0, areas_clusters.shape[0]/25 + 5))
    ax4.set_ylim((0, areas_clusters.shape[1] + 20))

def hausdorff_pdist(X):
    # Adapted from scipy.spatial.distance.pdist()
    from scipy.spatial.distance import directed_hausdorff
    n = len(X)
    out_size = (n * (n - 1)) // 2
    dm = np.empty(out_size, dtype=np.double)
    k = 0
    for i in range(n - 1):
        for j in range(i + 1, n):
            dm[k], _, _ = directed_hausdorff(X[i], X[j])
            k += 1
    return dm

def histOutline(dataIn, *args, **kwargs):
    (histIn, binsIn) = np.histogram(dataIn, *args, **kwargs)

    stepSize = binsIn[1] - binsIn[0]

    bins = np.zeros(len(binsIn)*2 + 2, dtype=np.float)
    data = np.zeros(len(binsIn)*2 + 2, dtype=np.float)
    for bb in range(len(binsIn)):
        bins[2*bb + 1] = binsIn[bb]
        bins[2*bb + 2] = binsIn[bb] + stepSize
        if bb < len(histIn):
            data[2*bb + 1] = histIn[bb]
            data[2*bb + 2] = histIn[bb]

    bins[0] = bins[1]
    bins[-1] = bins[-2]
    data[0] = 0
    data[-1] = 0
    return (bins, data)

def getContTable(ar1, ar2):
    cont = {}
    for i in range(0, len(ar1)):
        keyAr1 = ar1[i]
        keyAr2 = ar2[i]
        if keyAr1 in cont:
            if keyAr2 in cont[keyAr1]:
                cont[keyAr1][keyAr2] += 1
            else:
                cont[keyAr1][keyAr2] = 1
        else:
            cont[keyAr1] = {keyAr2: 1}
    return cont


def getContTableTotals(cont, ar1, ar2):
    sumRow = {}
    sumCol = {}
    h1 = set(ar1)
    h2 = set(ar2)
    for x in h2:
        sumRow[x] = 0
        for y in h1:
            if y in cont:
                if x in cont[y]:
                    val = cont[y][x]
                    sumRow[x] += val
                    if y in sumCol:
                        sumCol[y] += val
                    else:
                        sumCol[y] = val
    total = 0
    for x in h1:
        total += sumCol[x]
    return (sumRow, sumCol, total)


def getMismatchMatrix(cont, ar1, ar2):
    totals = getContTableTotals(cont, ar1, ar2)
    # print totals
    n = totals[2]
    h1 = set(ar1)
    h2 = set(ar2)
    a = 0
    for x in h1:
        for y in h2:
            if x in cont:
                if y in cont[x]:
                    val = cont[x][y]
                    a += (val * (val - 1)) / 2
    a1 = 0
    sumCol = totals[1]
    for x in sumCol:
        val = sumCol[x]
        a1 += (val * (val - 1)) / 2

    b = a1 - a

    a2 = 0
    sumRow = totals[0]
    for x in sumRow:
        val = sumRow[x]
        a2 += (val * (val - 1)) / 2
    c = a2 - a
    d = float((n * (n - 1)) / 2) - a1 - c
    return (a, b, c, d, n)


def getRand(a, b, c, d):
    rand = (a + d) / float(a + b + c + d)
    return rand


def getWallace(a, b, c):
    w1 = float(0)
    w2 = float(0)
    if (a + b) > 0:
        w1 = a / float(a + b)
    if (a + c) > 0:
        w2 = a / float(a + c)
    return (w1, w2)


def sankey_plot(labels_chr1,id_s1,labels_chr2,id_s2):
    import plotly.graph_objects as go
    import plotly.io as pio
    pio.renderers.default = "browser"
    n1 = len(np.unique(labels_chr1))
    n2 = len(np.unique(labels_chr2))
    id_s1 = range(12)
    id_s2 = range(12)
    color1 = plt.cm.hsv(np.linspace(0, 1, len(np.unique(labels_chr1))))
    color2 = plt.cm.hsv(np.linspace(0, 1, len(np.unique(labels_chr2))))

    dis_mat = np.zeros((n1, n2))
    source = []
    target = []
    value = []
    clink = []
    j_mat = np.zeros((n1, n2))
    # for i in tqdm(range(n1)):
    #     for j in range(n2):

    for i in tqdm(id_s1):
        for j in id_s2:
            chrom_ind1 = np.where(labels_chr1 == i)[0]
            chrom_ind2 = np.where(labels_chr2 == j)[0]
            share_n = len(set(chrom_ind1).intersection(chrom_ind2))
            # ratio = share_n * 2 / (len(chrom_ind1) + len(chrom_ind2))
            ratio = share_n / (len(chrom_ind1) + len(chrom_ind2) - share_n)
            j_mat[i, j] = ratio
            ratio1 = share_n / len(chrom_ind1)
            ratio2 = share_n / len(chrom_ind2)
            source.append(i)
            target.append(j + n1)
            value.append(share_n)
            c = (color1[i] * 255)
            if ratio > .01:
                # clink.append('rgba' + str((c[0], c[1], c[2], 0.5)))
                clink.append("rgba(0, 0, 0, .5)")
            # if ratio > .05:
            #     # if ratio1>ratio2:
            #     #     c = (color1[i] * 255)
            #     # else:
            #     #     c = (color2[j] * 255)
            #     c = (color1[i] * 255)
            #     clink.append('rgba' + str((c[0], c[1], c[2],0.5)))
            #     # clink.append("rgba(255, 255, 255, .5)")
            #     # clink.append("rgba(0, 0, 0, .5)")
            else:
                clink.append("rgba(50, 50, 50, .1)")
    j_score = j_mat.max(axis=1).mean()
    cnode = []
    for i in color1:
        i = (i * 255)
        cnode.append('rgb' + str((i[0], i[1], i[2])))
    for i in color2:
        i = (i * 255)
        cnode.append('rgb' + str((i[0], i[1], i[2])))
    link = dict(source=source, target=target, value=value, color=clink)
    label = list(map(str, np.hstack((np.arange(0, n1), np.arange(0, n2)))))
    node = dict(label=label, pad=5, thickness=5, color=cnode)
    data = go.Sankey(textfont=dict(color="rgba(0,0,0,0)", size=1), link=link, node=node)
    # print(data)
    layout = go.Layout(
        paper_bgcolor='rgba(0,0,0,0)',
        plot_bgcolor='rgba(0,0,0,0)'
    )
    fig = go.Figure(data=data, layout=layout)
    return fig,j_score

def get_dwelltime(speed):
    speed2 = speed.copy()
    # win = round(len(speed2) / 100)*2+1
    speed2 = smooth(speed2, 25)
    # speed2 = smooth(speed2, 155)
    trough = find_trough(speed2, 55, 75, 0)
    # trough = fs.merge_trough(trough,points_sub,speed,10)
    # trough = np.hstack((0, trough2, len(speed)))
    vs_areas = speed
    rise = []
    fall = []
    peaks = []
    trough3 = []
    for i in range(len(trough) - 1):
        peak = vs_areas[trough[i]:trough[i + 1]].argmax() + trough[i]
        if peak != trough[i] and peak != trough[i+1]:
            peaks.append(peak)
            rising = vs_areas[trough[i]:peak]
            cut = np.quantile(rising, 0.75)
            # cut = (rising[-1] + rising[0]) / 2
            rise.append(np.abs(rising - cut).argmin() + trough[i])
            falling = vs_areas[peak:trough[i + 1]]
            # cut = (falling[0] + falling[-1]) / 2
            cut = np.quantile(falling, 0.75)
            fall.append(np.abs(falling - cut).argmin() + peak)
            trough3.append(trough[i])

    dwell=[]
    for i in trough3:
        if i < rise[-1]:
            rise2=rise[np.where((rise - i)>0)[0][0]]
        else:
            rise2 = i
        if i > fall[0]:
            fall2=fall[np.where((i-fall)>0)[0][-1]]
        else:
            fall2 = i
        dwell.append(rise2 - fall2)
    return(trough3,dwell)

    # plt.plot(rise, vs_areas[rise], 'o')
    # plt.plot(fall, vs_areas[fall], 'o')


def circular_hist(ax, x, bins=16, density=True, offset=0, gaps=True, color='k',ymax = 0):
    """
    Produce a circular histogram of angles on ax.

    Parameters
    ----------
    ax : matplotlib.axes._subplots.PolarAxesSubplot
        axis instance created with subplot_kw=dict(projection='polar').

    x : array
        Angles to plot, expected in units of radians.

    bins : int, optional
        Defines the number of equal-width bins in the range. The default is 16.

    density : bool, optional
        If True plot frequency proportional to area. If False plot frequency
        proportional to radius. The default is True.

    offset : float, optional
        Sets the offset for the location of the 0 direction in units of
        radians. The default is 0.

    gaps : bool, optional
        Whether to allow gaps between bins. When gaps = False the bins are
        forced to partition the entire [-pi, pi] range. The default is True.

    Returns
    -------
    n : array or list of arrays
        The number of values in each bin.

    bins : array
        The edges of the bins.

    patches : `.BarContainer` or list of a single `.Polygon`
        Container of individual artists used to create the histogram
        or list of such containers if there are multiple input datasets.
    """
    # Wrap angles to [-pi, pi)
    x = (x+np.pi) % (2*np.pi) - np.pi

    # Force bins to partition entire circle
    if not gaps:
        bins = np.linspace(-np.pi, np.pi, num=bins+1)

    # Bin data and record counts
    n, bins = np.histogram(x, bins=bins)

    # Compute width of each bin
    widths = np.diff(bins)

    # By default plot frequency proportional to area
    if density:
        # Area to assign each bin
        area = n / x.size
        # Calculate corresponding bin radius
        radius = (area/np.pi) ** .5

        radius = n/ n.max()
    # Otherwise plot frequency proportional to radius
    else:
        radius = n

    # Plot data on ax
    patches = ax.bar(bins[:-1], radius, zorder=1, align='edge', width=widths,edgecolor='k',
                      color=color, alpha=0.5,linewidth=1)

    # Set the direction of the zero angle
    ax.set_theta_offset(offset)
    ax.set_rlabel_position(np.rad2deg(bins[n.argmin()]))
    if radius.max()<10:
        ax.set_rticks(np.arange(0, radius.max(), 1))
    else:
        ax.set_rticks(np.arange(0, radius.max(), 10))

    if ymax > 0:
        ax.set_rticks(np.arange(0, ymax, 50))
        ax.set_ylim((0, ymax))
    # Remove ylabels for area plots (they are mostly obstructive)
    if density:
        ax.set_yticks([])

    return n, bins, patches


def spline_reparam(dt, S, model=False):
    from scipy.interpolate import CubicSpline
    S_tot = S[-1]
    models = [CubicSpline(S, pc) for pc in dt.T]
    unit_x = np.arange(0, S_tot, 1)
    unit_y = np.array([model(unit_x) for model in models]).T
    if model:
        return unit_y, models
    else:
        return unit_y

def dtangent(dt, full=False):
    L = np.sum(velocity(d_F(dt, 1)))

    ddt = np.diff(dt, axis=0)
    ddt /= np.linalg.norm(ddt, axis=1)[:, np.newaxis] #normalized tangent vector
    dddt = np.diff(ddt, axis=0)
    res = np.sum(np.sqrt(np.sum(dddt**2, axis=1)))
    if full:
        return res / L, np.sqrt(np.sum(dddt**2, axis=1))
    return res / L


def get_speed_landmarks(s, factor, trim=0):
    s = s[trim:-1 - trim]
    p1 = np.argmin(s[:])
    p2 = np.argmax(s[:])
    print('p1 = {}, p2 = {}, /{}'.format(p1, p2, len(s)))

    mu = s.mean()
    std = s.std()


    try:
        # start = np.where(s[:p1] >= factor * s[p1])[0][-1]
        start = np.where(s[:p2] < mu + factor * std)[0][-1]

    except:
        start = trim
        print('EXCEPT: start could not be found')

    try:
        # stop = p2 + np.where(s[p2:] <= factor * s[p2])[0][0]
        stop = p2 + np.where(s[p2:] < mu + factor * std)[0][0]

    except:
        stop = len(s)
        print('EXCEPT: stop could not be found')
    print('start = {}, p1 = {}, p2 = {}, stop = {} /{}'.format(start, p1, p2, stop, len(s)))
    return p1 + trim, p2 + trim, start + trim, stop + trim


def minmax(array):
    return (array - array.min(axis=0)) / (array.max(axis=0) - array.min(axis=0))

##

