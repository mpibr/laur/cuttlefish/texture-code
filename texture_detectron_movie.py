import os
import matplotlib.pyplot as plt
# from keras.models import load_model
import numpy as np
from PIL import Image
import skimage.measure
from tqdm import tqdm
import skimage.transform
import cv2
import umap
from tensorflow.keras.applications import vgg19
home = os.environ['HOME']
os.chdir(home+'/Dropbox/PycharmProjects/cep_stim/')
import featurespace as fs
import util
from keras.models import load_model
import h5py
from sklearn import preprocessing
from sklearn.decomposition import PCA
import joblib
import imageio
from importlib import reload
reload(fs)
import argparse
from mpl_toolkits.mplot3d import Axes3D


#####load models
args_cuttlefish_model = home + '/Dropbox/lab2/model_final.pth'
args_cuttlefish_model_dt2_base = 'COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x.yaml'
cuttlefish_model = util.load_detectron2_model(
    args_cuttlefish_model, args_cuttlefish_model_dt2_base,
    num_class=2, score_threshold=0.85, use_gpu=False)

args_rotate_model = home + '/Dropbox/lab2/model_final_r.pth'
args_rotate_model_dt2_base = 'COCO-Keypoints/keypoint_rcnn_R_50_FPN_3x.yaml'


rotate_model = util.load_detectron2_model(
    args_rotate_model, args_rotate_model_dt2_base,
    num_kp = 2, score_threshold = 0.85, use_gpu = False)

cuttlefish_model_type='detectron2'
fill_value=0
rotate_model_type='detectron2'

model = vgg19.VGG19(weights='imagenet',include_top=False)
model.compile(loss="categorical_crossentropy", optimizer="adam")

#####load video

if __name__ == '__main__':
    p = argparse.ArgumentParser()
    p.add_argument('input')
    p.add_argument('--start', default=0, type=int)
    p.add_argument('--end', type=int)
    p.add_argument('--sampling_rate', default=1,type=int, help='Sampling rate (every n frames)')
    p.add_argument('--r', default=700, type=int)
    p.add_argument('--detectron_heading', default=0, type=int)
    p.add_argument('--reg_heading', default=1, type=int)
    p.add_argument('--umap', default=1, type=int)
    p.add_argument('--pca', default=1, type=int)
    p.add_argument('output1')
    p.add_argument('output')
    args = p.parse_args()

# cuttlefish_radius=700
# detectron_heading=0
# reg_heading=1
#
#     exp='sepia207-20200320-002'
#     # exp='rec2_cam0_2020-04-15-15-15-45-10X.mp4'
#     date='2020-04-30-1'
#    
#     input='/gpfs/laur/sepia_compressed/'+ exp + '/'
#     # input  = home + '/Desktop/camouflage/'
#     output=home+'/Dropbox/lab2/sepia/' + exp + '/'

    input = os.path.dirname(args.input) + '/'
    output = os.path.dirname(args.output) + '/'
    output1 = os.path.dirname(args.output1) + '/'

    cuttlefish_radius=args.r
    registration = args.detectron_heading + args.reg_heading


    file_name = [fn for fn in os.listdir(input)
                  if 'rec2_cam0' in fn]
    print(input+file_name[0])


    start_frame = args.start
    cap = cv2.VideoCapture(input+file_name[0])
    if args.end is None:
        end_frames = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
    else:
        end_frames = args.end

    num_frames = end_frames - start_frame
    skip=args.sampling_rate
    n_point = num_frames // skip
    if skip == 1:
        seg_name = output1 +file_name[0][10:-10]+'overview'+str(start_frame) + '-' + str(end_frames) + '.seg'
    else:
        seg_name = output1 +file_name[0][10:-10]+'overview'+str(start_frame) + '-' + str(end_frames) \
                   + 'skip' + str(skip) + '.seg'
    windowsize = cuttlefish_radius*2


    try:
        texture_file = h5py.File(seg_name, 'r+')
        reg = texture_file['reg']
        vecRep = texture_file['texture']
        area = texture_file['area']
        masked = reg[0,...]
        file_exist = 1
    except:
        file_exist = 0
        texture_file = h5py.File(seg_name, 'w')
        vecRep = texture_file.create_dataset('texture', shape=(n_point, 512),
                                             dtype='float', compression='gzip')
        reg = texture_file.create_dataset('reg', shape=(n_point, windowsize, windowsize, 3),
                                          dtype='uint8', compression='gzip')
        area = texture_file.create_dataset('area', shape=(n_point, 1),
                                           dtype='float', compression='gzip')
    try:
        img0 = Image.open(output + 'img0.tif')
        img0 = np.array(img0)
    except:
        pass
    
    for n in tqdm(range(n_point)):
        if area[n]==0:
            frame = start_frame + n * skip
            cap.set(cv2.CAP_PROP_POS_FRAMES, frame)
            succ, img = cap.read()
            if succ:
                im, mask, coords = util.get_cropped_cuttlefish_and_mask(
                    img[:,:,::-1], cuttlefish_model, r=cuttlefish_radius, predict_small=True, return_xy=True,
                    model_type=cuttlefish_model_type)
                masked = im * np.expand_dims(mask, axis=2)
                if mask.sum() == 0:
                    im, mask, coords = util.get_cropped_cuttlefish_and_mask(
                        img, cuttlefish_model, r=cuttlefish_radius, predict_small=True, return_xy=True,
                        model_type=cuttlefish_model_type)
                    im = im[:, :, ::-1]
                    masked = im * np.expand_dims(mask, axis=2)

                if mask.sum() > 0 and registration>0:
                    if args.detectron_heading:
                        try:
                            rotated, rotated_mask = util.rotate_ellipse(
                                mask, masked, upright_model=rotate_model, fill_value=fill_value,
                                model_type=rotate_model_type, return_mask=True)
                            masked = rotated * np.expand_dims(rotated_mask, axis=2)
                        except:
                            print('cannot find head for frame',str(n))

                    elif args.reg_heading:
                        labels = skimage.measure.label(mask, connectivity=2)
                        props = fs.find_the_large_mask(labels, masked[:,:,0])
                        img1 = fs.center_rotated(masked, props, cuttlefish_radius*2)


                        if 'img0' in globals():
                            # img2=fs.histN(img1)
                            (masked, warp) = fs.rigid_reg(img1, img0,1,64)
                            if n == 0:
                                img0 = masked
                            # masked = cv2.warpAffine(img1, warp, (img1.shape[:2]),
                            #                       flags=cv2.INTER_LINEAR + cv2.WARP_INVERSE_MAP)
                        else:
                            # img0 = fs.histN(img1)
                            img0 = img1
                            masked = img1
                            img0_save = Image.fromarray(img0)
                            img0_save.save( output + 'img0.tif')

                    reg[n, ...] = masked
                    masked_H = fs.histN(masked)
                    vecRep[n] = fs.to_tex(masked_H, model,224)
                    area[n] = (mask > 0).sum()
                
                
    # if not file_exist:
    vecRep2 = np.array(vecRep)
    area2 = np.array(area)
    joblib.dump((vecRep2, area2), output + os.path.basename(seg_name)[:-4] + '_vecRep')

    reg2 = np.array(reg)
    filtered = np.squeeze((area2 > (area2.mean() - area2.std())) * (area2 < (area2.mean() + area2.std())))
    vecRep2 = vecRep2[filtered, :]
    reg2 = reg2[filtered, ...]


##################################################################UMAP
    if args.umap:
        skip_fit = 2
        n_neighbors = np.int(vecRep2.shape[0] // (skip_fit + 1))
        n_neighbors = 15
        min_dist = 0.0
        reducer = umap.UMAP(n_neighbors=n_neighbors,
                            min_dist=min_dist, random_state=42).fit(vecRep2[::skip_fit, :])
        embedding = reducer.transform(vecRep2)
        print('Umap:', embedding.shape)
        plt.close('all')
        plt.figure()
        d1 = 0
        d2 = 1
        cm = plt.cm.get_cmap('RdYlBu')
        z = range(0, embedding.shape[0])
        plt.scatter(embedding[:, d1], embedding[:, d2],
                    vmin=0, vmax=embedding.shape[0], s=20, c=z, cmap=cm)
        plt.plot(embedding[:, d1], embedding[:, d2], linewidth=0.2, c='k')
        plt.savefig(output + os.path.basename(seg_name)[:-4] +
                    '-trace-Umap' + str(n_neighbors) + '-' + str(min_dist) + '.png', dpi=300)
        fs.plot_atlas(embedding, reg2)
        plt.savefig(output + os.path.basename(seg_name)[:-4] +
                    '-trace-Umap' + str(n_neighbors) + '-' + str(min_dist) + 'atlas.png', dpi=300)


##################################################################PCA
    if args.pca:
        pca2 = PCA(n_components=3, svd_solver='full')
        texPCA = pca2.fit(vecRep2)
        text_points = texPCA.transform(vecRep2)
        fig = plt.figure()
        ax = fig.gca(projection='3d')
        ax.plot(text_points[:, 0], text_points[:, 1], text_points[:, 2], linewidth=1)
        plt.savefig(output+os.path.basename(seg_name)[:-4] + '-PCA.png',dpi=300)

    if registration:
        imageio.mimwrite(output+os.path.basename(seg_name)[:-4] + '.mp4', reg, fps=25)

    texture_file.close()

