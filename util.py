#!/usr/bin/env python3
# -*- encoding: utf-8 -*-
"""
Functions that are used fin the texture analysis pipeline.
@author: woot
"""

import os
from glob import glob
import numpy as np
import cv2
import h5py

def pad_and_crop(im, x, y, r):  
    """Crop around x, y by r radius, pad black if not there."""
    
    x = int(np.round(x))
    y = int(np.round(y))
    
    if len(im.shape) == 2:
        new_im = np.zeros((r*2,r*2),dtype='uint8')
    else:
        new_im = np.zeros((r*2,r*2,3),dtype='uint8')
    
    src_h, src_w = im.shape[:2]
    src_y0, src_x0 = (y-r, x-r)
    src_y1, src_x1 = (y+r, x+r)
    
    trg_h, trg_w = new_im.shape[:2]
    trg_y0, trg_x0 = (0, 0)
    trg_y1, trg_x1 = trg_h, trg_w
    
    if src_x0 < 0:
        src_x0 = 0
        trg_x0 = r-x
        
    if src_y0 < 0:
        src_y0 = 0
        trg_y0 = r-y
    
    if src_x1 > src_w:
        trg_x1 = trg_w - (src_x1 - src_w)
        src_x1 = src_w
        
    if src_y1 > src_h:
        trg_y1 = trg_h - (src_y1 - src_h)
        src_y1 = src_h
    
    new_im[trg_y0:trg_y1, trg_x0:trg_x1] = im[src_y0:src_y1, src_x0:src_x1]
    
    return new_im

def predict_cuttlefish(im, model, predict_small=False):
    """
    Take any size BGR image.
    Return: mask of cuttlefish==1 same size, centroid x, centroid y."""

    im_original = im.copy()
    h, w = im.shape[:2]
    if predict_small:
        if (h == 3000) and (w == 3000):
            im = cv2.resize(im, (128,128), interpolation=cv2.INTER_LINEAR)
        else:
            im = cv2.resize(im, None, fx=0.05, fy=0.05, interpolation=cv2.INTER_LINEAR)
    
    # Use model
    x = im.reshape((1,) + im.shape)
    yhat = model.predict(x, verbose=0)[...,::-1]
    yhat_max = np.argmax(yhat, -1).squeeze().astype('uint8')
    
    if predict_small:
        yhat_max = cv2.resize(yhat_max, (im_original.shape[1],im_original.shape[0]), 
                              interpolation=cv2.INTER_LINEAR)

    # Choose cuttlefish contour
    mask = np.zeros(yhat_max.shape[:2],dtype='uint8')
    contours, _ = cv2.findContours(yhat_max, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    areas = np.array([cv2.contourArea(cnt) for cnt in contours])
    contours = np.array(contours)[areas > 0.00777*(yhat_max.shape[0]*yhat_max.shape[1])]
    if len(contours) > 0:
        cuttlefish_contour_id = np.argmax([4*np.pi*cv2.contourArea(cnt)/(cv2.arcLength(cnt, True)**2) \
                                           for cnt in contours])
        cv2.drawContours(mask, contours, cuttlefish_contour_id, 1, -1)
        x, y = contours[cuttlefish_contour_id].mean(axis=0).T
    else:
        x = None
        y = None
    return mask, x, y

def load_detectron2_model(saved_weights, 
    baseline='COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x.yaml', 
    num_class=2, num_kp=2, score_threshold=0.85, use_gpu=False):
    """
    Load config from model zoo
    
    saved_weights : path to saved model checkpoint
    baselines : pretrained baseline model, https://github.com/facebookresearch/detectron2/blob/master/MODEL_ZOO.md
    
    Return default predictor
    """
    
    import detectron2
    from detectron2.config import get_cfg
    from detectron2 import model_zoo
    from detectron2.engine import DefaultPredictor
    
    cfg = get_cfg()
    
    cfg.merge_from_file(model_zoo.get_config_file(baseline))
    cfg.MODEL.WEIGHTS = saved_weights
    cfg.MODEL.ROI_HEADS.NUM_CLASSES = num_class
    cfg.MODEL.ROI_KEYPOINT_HEAD.NUM_KEYPOINTS = num_kp
    cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = score_threshold
    if not use_gpu:
        cfg.MODEL.DEVICE = 'cpu'
    
    predictor = DefaultPredictor(cfg)
    
    return predictor

def predict_cuttlefish_detectron2(im, model, label_class=0):
    """
    Take any size BGR image.
    Return: mask of one cuttlefish==1 same size, centroid x, centroid y."""
    
    outputs = model(im)

    instances = outputs["instances"].to("cpu")
    instances[instances.pred_classes == label_class]
    if len(instances.scores.numpy()) == 0:
        mask = np.zeros(im.shape[:2], dtype='uint8')
        x = None
        y = None
    else:
        best_score_index = np.argmax(instances.scores.numpy())

        mask = instances.pred_masks.numpy()[best_score_index]
        mask = mask.astype('uint8')
        
        bbox = instances.pred_boxes.tensor.numpy()[best_score_index]
        x, y = bbox.reshape(2,2).mean(axis=0)

    return mask, x, y

def calcFocus(img):
    redFrame=np.single(img[:,:,2])
    gSigma1=2
    gSize1=15
    gSigma2=1
    gSize2=15
    blur1 = cv2.GaussianBlur(redFrame, (gSize1,gSize1), gSigma1)
    blur2 = cv2.GaussianBlur(redFrame, (gSize2,gSize2), gSigma2)
    blur3=blur1-blur2
    mask = np.zeros_like(blur3, dtype=bool)
    mask[blur3>4] = True
    focusV=np.sum(mask[:])

    return focusV, mask

def img2mask(im, percentile=50):
    from skimage import morphology
    from scipy import ndimage
    
    gSigma1=2
    gSize1=31
    gSigma2=1
    gSize2=31
    gSigma3=30
    gSize3=int(2*(2*gSigma3)+1)
    
    minSize=1000000
    foc, mask = calcFocus(im)
    blur1 = cv2.GaussianBlur(mask.astype('uint8'), (gSize1,gSize1), gSigma1)
    blur2 = cv2.GaussianBlur(mask.astype('uint8'), (gSize2,gSize2), gSigma2)
    blur3=blur1-blur2
    blur3 = cv2.GaussianBlur(blur3*mask, (gSize3,gSize3), gSigma3)
    mask = np.zeros_like(blur3, dtype=bool)
    mask[blur3>np.percentile(blur3, percentile)] = True
    cleaned = morphology.remove_small_objects(mask, minSize, 2)
    labels, num_labels = ndimage.label(cleaned)
    if num_labels==0:
        cmask = np.ones_like(labels, dtype='bool_')
        return cmask.astype('uint8'), (None, None)
    else:
        cuttlefish_label = np.argmax(np.bincount(labels.flatten())[1 : ]) + 1
        mask = (labels == cuttlefish_label).astype(int)
        com = ndimage.measurements.center_of_mass(mask)
        return mask.astype('uint8'), com
    

def get_cropped_cuttlefish_and_mask(im, model, r, model_type='keras',
    predict_small=True, return_xy=False, percentile=50, target_im=None):
    """Take input cv2 image
    model_type : 'keras', 'detectron2'
    Return: cropped cuttlefish, cropped mask
    """

    if model_type == 'keras':
        mask, x, y = predict_cuttlefish(im, model, predict_small=predict_small)
    elif model_type == 'detectron2':
        mask, x, y = predict_cuttlefish_detectron2(im, model)
    elif model_type == 'dog':
        mask, (y, x) = img2mask(im, percentile)
    if (x is None) or (y is None):
        crop = np.zeros((r*2,r*2,3),dtype='uint8')
        mask = np.zeros((r*2,r*2),dtype='uint8')
        if return_xy:
            return crop, mask, np.array([np.nan, np.nan])
        else:
            return crop, mask
    
    if target_im is not None:
        im = target_im
    crop = pad_and_crop(im, x, y, r)
    crop_mask = pad_and_crop(mask, x, y, r)
    
    if return_xy:
        return crop, crop_mask, np.hstack((x,y))
    else:
        return crop, crop_mask

def rotate_ellipse(
    mask, original, angle_offset=0, upright_model=None, model_type='keras',
    predict_padded=True, origin='lower', return_length=False, return_mask=False, fill_value=0):
    """
    Fit an ellipse to mask (background=0, blob=1).
    model_type : 'keras', 'detectron2', 'corr'
    angle_offset : Additional angle offset, in degree.
    origin : 'upper' or 'lower', matches matplotlib.pyplot.imshow()

    Return : rotated original.
    If upright_model is None, assume head has few pixels than tail, point head upward.

    Detectron keypoint model, keypoint[0] = head / anterior
    """
    from scipy.signal import correlate

    contours, _ = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    
    contour_id = np.argsort([cv2.contourArea(x) for x in contours])[::-1][0]
    ellipse = cv2.fitEllipse(contours[contour_id])
    l = max(ellipse[1])
    x, y = ellipse[0]
    
    ma = np.zeros(mask.shape[:2],dtype='uint8')
    cv2.ellipse(ma, ellipse, 1, -1)

    roMat = cv2.getRotationMatrix2D(ellipse[0], ellipse[2], 1.0)

    if upright_model is None:
        mask_in_elipse = mask.copy()
        mask_in_elipse[ma==0] = 0
        mask_in_elipse = cv2.warpAffine(
            mask_in_elipse, roMat, mask_in_elipse.shape[:2])
        is_upright = len(mask_in_elipse[:int(y)].nonzero()[0]) < \
            len(mask_in_elipse[int(y):].nonzero()[0])
            
    elif upright_model == 'mantle':
        mask_in_elipse = mask.copy()
        mask_in_elipse[ma==1] = 0
        mask_in_elipse = cv2.warpAffine(
            mask_in_elipse, roMat, mask_in_elipse.shape[:2])
        is_upright = mask_in_elipse[:int(y)].astype('bool').sum() > \
        mask_in_elipse[int(y):].astype('bool').sum()

    elif model_type == 'detectron2':
        if predict_padded:
            h, w = original.shape[:2]
            img = np.ones((3000,3000,3), dtype='uint8') * fill_value
            img[:h,:w] = original
        else:
            img = original.copy()

        outputs = upright_model(img)
        instances = outputs["instances"].to("cpu")

        heads = instances.pred_keypoints.numpy()[:,0,:]
        if predict_padded:
            heads = heads[(heads[:,0] <= w) * (heads[:,1] <= h)]
        kp_mask = [mask.astype('bool')[int(np.round(y)), int(np.round(x))] \
            for x, y, _ in heads]
        heads_in_mask = heads[kp_mask]
        if len(heads_in_mask) > 0:
            head_best = heads_in_mask[np.argmax(heads_in_mask[...,2])]
            head_best_rotated = cv2.transform(np.array([[head_best[:2]]]), roMat).squeeze()

            head_y = head_best_rotated[1]
            midpt_y = original.shape[0] / 2
            is_upright = (head_y < midpt_y).astype('bool')
        else:
            mask_in_elipse = mask.copy()
            mask_in_elipse[ma==0] = 0
            mask_in_elipse = cv2.warpAffine(
                mask_in_elipse, roMat, mask_in_elipse.shape[:2])
            is_upright = len(mask_in_elipse[:int(y)].nonzero()[0]) > \
                len(mask_in_elipse[int(y):].nonzero()[0])

    elif model_type == 'keras':
        img = cv2.warpAffine(original, roMat, original.shape[:2])
        img = cv2.resize(img, (128,128))

        is_upright = np.argmax(
            upright_model.predict(img.reshape((1,) + img.shape).astype(float), 
                verbose=0)).astype('bool')
        
    if (model_type == 'corr') and (upright_model is not None):
        masked_ref = upright_model
        roMat_candidates = [
            cv2.getRotationMatrix2D(ellipse[0], ellipse[2]-angle_offset, 1.0),
            cv2.getRotationMatrix2D(ellipse[0], 180+ellipse[2]-angle_offset, 1.0)]
        img_candidates = [
            cv2.warpAffine(original, rm, original.shape[:2], 
                borderValue=(fill_value,fill_value,fill_value)) for rm in roMat_candidates]
        if masked_ref.shape == 3:
            preferred_idx = np.argmax([np.mean(correlate(masked_ref[...,0], im[...,0])) \
                for im in img_candidates])
        else:
            preferred_idx = np.argmax([np.mean(correlate(masked_ref, im)) \
                for im in img_candidates])
        roMat = roMat_candidates[preferred_idx]
        img = img_candidates[preferred_idx]
    else:
                
        if (is_upright and (origin == 'upper')) or (not is_upright and (origin == 'lower')) :
            roMat = cv2.getRotationMatrix2D(ellipse[0], ellipse[2]-angle_offset, 1.0)
        else:
            roMat = cv2.getRotationMatrix2D(ellipse[0], 180+ellipse[2]-angle_offset, 1.0)
            
        img = original.copy()
        img = cv2.warpAffine(img, roMat, img.shape[:2], 
            borderValue=(fill_value,fill_value,fill_value))

    to_be_returned = (img,)
    if return_mask:
        mask = cv2.warpAffine(mask, roMat, mask.shape[:2], borderValue=0)
        to_be_returned = to_be_returned + (mask,)
    if return_length:
        to_be_returned = to_be_returned + (l,)

    if len(to_be_returned) == 1:
        return to_be_returned[0]
    else:
        return to_be_returned

def get_cuttlefish_length(vidfile, cuttlefish_model, upright_model, 
        min_mask_fraction=0.07, n_samples=3, init_radius=700, 
        cuttleifsh_model_type='detectron2', upright_model_type='detectron2'):

    cap = cv2.VideoCapture(vidfile)
    num_frames = cap.get(cv2.CAP_PROP_FRAME_COUNT)

    cuttlefish_lengths = []

    while len(cuttlefish_lengths) < n_samples:
        f = np.random.randint(num_frames-1)
        cap.set(cv2.CAP_PROP_POS_FRAMES, f)
        ret, im = cap.read()
        if ret:
            im, mask = get_cropped_cuttlefish_and_mask(
                im, cuttlefish_model, r=init_radius, model_type=cuttleifsh_model_type)
            mask_fraction = mask.sum()/(mask.shape[0]*mask.shape[1])
            if  mask_fraction >= min_mask_fraction:
                _, _, length = rotate_ellipse(
                    mask, im, upright_model=upright_model, model_type=upright_model_type, 
                    return_length=True, return_mask=True, fill_value=127)
                cuttlefish_lengths.append(length)
            else:
                print('Mask fraction is too small: {:.03f}, reading another random frame.'.format(
                    mask_fraction), flush=True)
        else:
            print('Cannot read frame, trying another.')

    avg_length = np.median(cuttlefish_lengths)

    return avg_length

def cv2_to_vgg19_input(im):
    """input: cv2.imread() without color conversion."""
    return cv2_to_model_input(im, model='VGG19')

def cv2_to_model_input(im, model='VGG19', interpolation='nearest', greyscale=False):
    """input: cv2.imread() without color conversion."""
    import tensorflow as tf
    
    if interpolation == 'nearest':
        interp_code = cv2.INTER_NEAREST
    elif interpolation == 'linear':
        interp_code = cv2.INTER_LINEAR

    im = cv2.resize(im, (224,224), interpolation=interp_code)
    if greyscale:
        # Written in so we don't forget whether it's BGR2GRAY or RGB2GRAY.
        im = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
        im = cv2.cvtColor(im, cv2.COLOR_GRAY2BGR)
    else:
        im = cv2.cvtColor(im, cv2.COLOR_BGR2RGB)
    model_class_name = model.lower()
    im = getattr(tf.keras.applications, model_class_name).preprocess_input(im)

    return im


def make_pattern_map(X, vids=None, frames=None, radius=None, 
        cuttlefish_model=None, upright_model=None, model_type=None,
        max_dist=5, n_grid=(5,10), grid_size=(64,32),
        zoom_in=None, eq_hist=True, 
        rotated_list=None):
    """
    X (n_samples, n_features)
    zoom_in : (x, y, w, h)
    """
    import os
    import itertools    
    from tqdm import tqdm
    from sklearn.neighbors import KDTree
    
    n_rows, n_cols = n_grid
    grid_y, grid_x = grid_size

    # Identify nearest neighbour for each point in the grid
    if zoom_in is None:
        x1, y1 = X.max(axis=0)
        x0, y0 = X.min(axis=0)
        x1 -= (x1-x0)/n_cols/2
        x0 += (x1-x0)/n_cols/2
        y1 -= (y1-y0)/n_rows/2
        y0 += (y1-y0)/n_rows/2
    else:
        x1, y1 = (zoom_in[0] + zoom_in[2], zoom_in[1] + zoom_in[3])
        x0, y0 = (zoom_in[0], zoom_in[1])
    xx, yy = np.meshgrid(np.linspace(x0, x1, n_cols), np.linspace(y0, y1, n_rows))
    tree = KDTree(X, leaf_size=2)
    dist, ind = tree.query(np.vstack((xx.ravel(), yy.ravel())).T, k=1)
    dist = dist.reshape(n_grid)
    ind = ind.reshape(n_grid)
    
    # Build grid of nearest neighbours
    grid_new = np.zeros((n_rows*grid_y, n_cols*grid_x, 3), dtype='uint8')
    for i, j in tqdm(itertools.product(np.arange(n_rows), np.arange(n_cols)), total=n_rows*n_cols):
        if dist[i,j] <= max_dist:
            idx = ind[i,j]

            if rotated_list is None:
                vid = vids[idx]
                cuttlefish_radius = radius[idx]
                f = frames[idx]

            if (rotated_list is None) and (not os.path.isfile(vid)):
                print('not exist:', vid)
            else:
                if rotated_list is None:
                    cap = cv2.VideoCapture(vid)
                    cap.set(cv2.CAP_PROP_POS_FRAMES, f)
                    _, im = cap.read()
                    
                    rotated, rotated_mask = crop_rotate(
                        im, cuttlefish_model, upright_model, 
                        cuttlefish_model_type=model_type, upright_model_type=model_type,                                
                        fill_value=0, r=cuttlefish_radius, eq_hist=eq_hist)

                else:
                    rotated = rotated_list[idx]

                if rotated is not None:

                    im = rotated

                    h, w = im.shape[:2]
                    im = cv2.resize(im, (max(grid_size), max(grid_size)))
                    margin = np.abs(grid_y-grid_x)//2
                    if grid_y > grid_x:
                        im = im[:, margin:-margin]
                    elif grid_x > grid_y:
                        im = im[margin:-margin, :]
                    grid_new[i*grid_y:(i+1)*grid_y,j*grid_x:(j+1)*grid_x] = im
                    
    x_offset = (x1 - x0) / n_cols / 2
    y_offset = (y1 - y0) / n_rows / 2
    mat_extent = (x0-x_offset, x1+x_offset, y0-y_offset, y1+y_offset)
    mat_aspect = ((x1-x0)/(y1-y0))/((n_cols*grid_x)/(n_rows*grid_y))
                    
    return grid_new, mat_extent, mat_aspect


def crop_rotate(im, cuttlefish_model, upright_model, 
    cuttlefish_model_type='keras', upright_model_type='keras', 
    fill_value=0, r=500, 
    mask_im=True, eq_hist=True):
    
    im, mask, coords = get_cropped_cuttlefish_and_mask(
        im, cuttlefish_model, r=r, predict_small=True, return_xy=True,
        model_type=cuttlefish_model_type)
    
    masked_area = (mask==1).sum()

    if masked_area > 0:
        rotated, rotated_mask = rotate_ellipse(
            mask, im, upright_model=upright_model, fill_value=fill_value,
            model_type=upright_model_type, return_mask=True)  # Same fill 
        
        if eq_hist:
            rotated[rotated_mask==0] = 0
            rotated = cv2.cvtColor(rotated, cv2.COLOR_BGR2GRAY)
            rotated = cv2.equalizeHist(rotated)
            rotated = cv2.cvtColor(rotated, cv2.COLOR_GRAY2BGR)
        
        if mask_im:
            rotated[rotated_mask==0] = fill_value  # Fill masked pixel
        
        return rotated, rotated_mask
    else:
        return None, None