#!/usr/bin/env python3
# -*- encoding: utf-8 -*-

import os
import shutil
import argparse
import datetime
import h5py
import numpy as np
import cv2
import matplotlib.pyplot as plt
from tqdm import tqdm
import util


if __name__ == '__main__':
    p = argparse.ArgumentParser('Generate trajectory movie.')
    p.add_argument('--output', help='Output video file.')
    p.add_argument('--tail-length', type=int, default=100, 
        help='Number of historic points to plot.')
    p.add_argument('--playback-factor', type=float, default=4,
        help='Playback speed of video expressed in multiple of sampling rate of trajectory.')
    p.add_argument('--cmap', default='Reds', 
        help='Matplotlib colormap name for plotting historic points.')
    p.add_argument('--codec', default="MP4V", help='Output video codec.')
    p.add_argument('--max-frames', type=int, 
        help='Maximum number of frames to play.')
    p.add_argument('--rerun', action='store_true', help='Overwrite existing output.')
    p.add_argument('trajectory', help='Input trajectory file (2D).')
    args = p.parse_args()

    if os.path.isfile(args.output) and not args.rerun:
        raise RuntimeError('Output already exists. Use flag --rerun to overwrite.')

    # Read inputs
    with h5py.File(args.trajectory, 'r') as hf:
        n_grid = hf.attrs['n_grid']
        grid_size = hf.attrs['grid_size']
        videofile = hf.attrs['video']
        videofile = os.path.abspath(
            os.path.join(os.path.dirname(hf.filename), videofile))
        im_grid = hf['im_grid'][:]
        trajectory = hf['trajectory'][:]
        mat_extent = hf.attrs['mat_extent']
        mat_aspect = hf.attrs['mat_aspect']
        sampling_rate = hf.attrs['sampling_rate']
        frame_start = hf.attrs['frame_start']
        frame_end = hf.attrs['frame_end']

    n_rows, n_cols = n_grid
    grid_y, grid_x = grid_size
    
    cap = cv2.VideoCapture(videofile)
    cap.set(1, frame_start)
    fps = cap.get(cv2.CAP_PROP_FPS)
    playback_speed = args.playback_factor * sampling_rate
    total_frames = int(frame_end - frame_start)
    if args.max_frames is not None:
        total_frames = int(args.max_frames * playback_speed)

    # Set up canvas
    width_ratios = (n_cols*grid_x)/(n_rows*grid_y)
    fig, ax = plt.subplots(1, 2, figsize=(n_cols*grid_x/grid_y+n_rows,n_rows), 
        gridspec_kw={'width_ratios': [width_ratios, 1]})
    util.remove_padding()

    # Setup with dummy data
    ax[0].matshow(im_grid, extent=mat_extent, origin='lower', aspect=mat_aspect)
    sc = ax[0].scatter(
        np.ones(args.tail_length)*trajectory[0,0], 
        np.ones(args.tail_length)*trajectory[0,1],
        s=80, c=np.linspace(0.1, 1, args.tail_length), 
        cmap=args.cmap, alpha=0.6, marker='o', lw=0, vmin=0.1, vmax=1)
    util.remove_lines()
    fr = ax[1].imshow(np.zeros((3000,3000)))

    # Tmp dir to write frames into.
    tmp_out_dir = os.path.join(os.path.dirname(args.output), 'tmp')
    if not os.path.isdir(tmp_out_dir):
        os.mkdir(tmp_out_dir)

    # Update each frame and write to files.
    pbar = tqdm(total=int(total_frames/playback_speed))
    i = 0
    for f in range(total_frames):
        if f%playback_speed == 0:
            frame_position = frame_start + f
            idx = int(frame_position // sampling_rate)
            if idx >= args.tail_length:
                points = trajectory[idx:idx-args.tail_length:-1][::-1]
            else:
                points = np.ones((args.tail_length, 2))*trajectory[0]
                existing_data = trajectory[idx::-1][::-1]
                points[:len(existing_data)] = existing_data
            sc.set_offsets(points)
            _, im = cap.read()
            im = cv2.cvtColor(im, cv2.COLOR_RGB2BGR)
            fr.set_data(im)
            ax[1].set_title(str(datetime.timedelta(seconds=frame_position/fps))[:7], 
                fontsize=20)

            fig.savefig(os.path.join(tmp_out_dir, '{:04d}.png'.format(i)), 
                    dpi=96, bbox_inches='tight', pad_inches=0.2)

            pbar.update(1)
            i += 1
        else:
            cap.grab()

    # Collect frames into video and remove tmp dir afterwards.
    os.system('ffmpeg -y -r {} -f image2 -i {} -qscale:v 3 {}'.format(
        fps, os.path.join(tmp_out_dir, '%04d.png'), args.output))
    shutil.rmtree(tmp_out_dir)