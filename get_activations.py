#!/usr/bin/env python3
# -*- encoding: utf-8 -*-

import os
import sys
import argparse
import random
import string
import numpy as np
from tqdm import tqdm
import h5py
import cv2
from mpi4py import MPI
import tensorflow as tf
from tensorflow.python.keras.models import load_model
import keract
import util

TQDM_TAG = 42

def translate(img, shift, axis=0):

    translated = np.ones_like(img, dtype='uint8')

    if axis > 1:
        raise ValueError(
            'Only vertical (axis=0) and horizontal (axis=1) translation are supported.')

    if shift > 0:
        if axis == 0:
            translated[shift:] = img[:-shift]
        elif axis == 1:
            translated[:, shift:] = img[:, :-shift]

    elif shift < 0:
        if axis == 0:
            translated[:shift] = img[-shift:]
        elif axis == 1:
            translated[:, :shift] = img[:, -shift:]
    
    else:
        translated = img.copy()

    return translated

def rotate(img, angle, fill_value=127):

    mid_y, mid_x = np.array(img.shape[:2]) // 2
    roMat = cv2.getRotationMatrix2D((mid_x, mid_y), angle, scale=1.0)
    
    rotated = cv2.warpAffine(img.copy(), roMat, img.shape[:2], 
        borderValue=(fill_value,fill_value,fill_value))

    return rotated

def blur(img, kernel_size, axis=0):

    kernel = np.zeros((kernel_size, kernel_size))

    if axis == 0:
        kernel[:, int((kernel_size - 1)/2)] = np.ones(kernel_size)
    elif axis == 1:
        kernel[int((kernel_size - 1)/2), :] = np.ones(kernel_size)
    else:
        raise ValueError(
            'Only vertical (axis=0) and horizontal (axis=1) blurs are supported.')

    kernel = kernel / kernel_size
    blurred = cv2.filter2D(img, -1, kernel) 

    return blurred

def get_activations(cap, pretrained_model, layer, sampling_rate, \
    samples_chuck_start, samples_chuck_end, \
    cuttlefish_model, rotate_model, \
    cuttlefish_radius, output_file, mode, n_features, 
    eq_hist, fill_value=127, ellipse=False, \
    cuttlefish_model_type='keras', rotate_model_type='keras', \
    shifts=None, angles=None, kernels=None, batch_size=100):
    """
    batch_size : number of images to pass into pretrained model at one time.
    """

    # Count the number of sets of images including augmentated ones
    aug_set_size = 1
    if shifts is not None:
        aug_set_size += len(shifts)*2
    if angles is not None:
        aug_set_size += len(angles)
    if kernels is not None:
        aug_set_size += len(kernels)*2

    # Set up worker node tmp file
    n_samples = samples_chuck_end - samples_chuck_start
    with h5py.File(output_file, 'w') as hf:
        hf.create_dataset('activations', (0, aug_set_size, n_features), dtype='float32', 
            maxshape=(n_samples, aug_set_size, n_features))
        hf.attrs['samples_chuck_start'] = samples_chuck_start
        hf.attrs['samples_chuck_end'] = samples_chuck_end

    # Move to begining of chunk
    chunk_start_f = samples_chuck_start * sampling_rate
    cap.set(cv2.CAP_PROP_POS_FRAMES, chunk_start_f)

    # Read single frames (and augment)
    coords_list = []
    areas_list = []
    frames_list = []
    
    if rotate_model_type == 'corr':
        rotate_model = None

    num_batches = np.ceil(n_samples / (batch_size//aug_set_size))
    for batch_samples in np.array_split(np.arange(n_samples), num_batches):
        preprocessed = []
        
        for _ in batch_samples:
            # Read frame and get mask
            _, im = cap.read()
            
            aug_list = []
            
            rotated, rotated_mask = util.crop_rotate(
                im, cuttlefish_model, upright_model=rotate_model,
                cuttlefish_model_type=cuttlefish_model_type,
                upright_model_type=rotate_model_type,
                r=cuttlefish_radius, 
                mask_im=True, fill_value=fill_value, 
                eq_hist=eq_hist)
            
            # If cuttlefish is detected in the frame
            if rotated is not None:
                aug_list += [rotated]  # original
                
                if ellipse:
                    h, w = rotated.shape[:2]
                    Ns = 72
                    ellipse_mask = np.zeros_like(rotated_mask)
                    ellipse_mask = cv2.ellipse(ellipse_mask, (w//2, h//2), (Ns*2, Ns*4),
                            0, 0, 360, (1,1,1), -1)
                    rotated[ellipse_mask==0] = fill_value  # Fill masked pixel
                
                
                aug_list += [rotated]  # original

                # Add augmented frames
                if shifts is not None:
                    aug_list += [translate(rotated, s, axis=0) for s in shifts]
                    aug_list += [translate(rotated, s, axis=1) for s in shifts]
                if angles is not None:
                    aug_list += [rotate(rotated, a, fill_value) for a in angles]
                if kernels is not None:
                    aug_list += [blur(rotated, k, axis=0) for k in kernels]
                    aug_list += [blur(rotated, k, axis=1) for k in kernels]

                # Preprocess original & augmented frames
                aug_pp = []   
                for aug in aug_list:
                    aug[rotated_mask==0] = fill_value  # Fill masked pixel again
                    pp = util.cv2_to_model_input(aug, pretrained_model.name)    
                    aug_pp.append(pp)

                # Get corresponding frame position before reading
                f = cap.get(cv2.CAP_PROP_POS_FRAMES) - 1

                preprocessed.append(aug_pp)
                coords_list.append(coords)
                areas_list.append(masked_area)
                frames_list.append(f)
                
                # Update progress bar
                if comm_size == 1:
                    PBAR.update(1)
                else:
                    comm.send(1, dest=0, tag=TQDM_TAG)
                    
                sys.stdout.flush()

            # Skip frames between samples
            for _ in range(sampling_rate-1):
                cap.grab()
        
        if len(preprocessed) > 0:
            # Get activations for the whole batch
            act = keract.get_activations(
                pretrained_model, np.array(preprocessed).reshape((-1,) + pp.shape), 
                layer_names=layer, auto_compile=True)
            actual_layer_name = list(act.keys())[0]  # Some add suffix to layer name
            act = act[actual_layer_name]
            if mode == 'max-pooled':
                act = act.max(axis=(1,2))
            elif mode == 'gram':
                # act = F (1,14,14,512)
                N = act.shape[-1]   # 512
                act = act.reshape(len(act), -1, N)  #  F (1,196,512)
                act = np.moveaxis(act, 2, 1)  #  F (1,512,196)
                M = act.shape[-1]  # 196
                # (512, 196) x (196, 512) = (512 x 512)
                act = np.array([np.dot(F, F.T).reshape(-1)/M for F in act])
            flatten = act.reshape(len(act), n_features)
            flatten = act.reshape(-1, aug_set_size, n_features)

            # Write activations batch by batch
            with h5py.File(output_file, 'r+') as hf:
                length = hf['activations'].shape[0]
                hf['activations'].resize(length + len(flatten), axis=0)
                hf['activations'][-len(flatten):] = flatten

    # Write other paramenters all in one go
    with h5py.File(output_file, 'r+') as hf:
        hf.create_dataset('areas', data=np.array(areas_list), dtype='int32')
        hf.create_dataset('positions', data=np.array(coords_list), dtype='float32')
        hf.create_dataset('frame_numbers', data=np.array(frames_list), dtype='int32')

if __name__ == '__main__':

    comm = MPI.COMM_WORLD
    comm_size = comm.Get_size()
    
    p = argparse.ArgumentParser(\
        'Get activations.', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    p.add_argument('--video', required=True, help='Video to process')
    p.add_argument('--cuttlefish-model', required=True, help='Path to segmentation model')
    p.add_argument('--rotate-model', help='Path to head tail model')
    p.add_argument('--model', default='VGG19', 
        help='Name of pretrained texture model.', 
        choices=[ m for m in dir(tf.keras.applications) if m[0].isupper()])
    p.add_argument('--cuttlefish-model-type', choices=['keras', 'detectron2'], default='keras')
    p.add_argument('--cuttlefish-model-dt2-base', 
        default='COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x.yaml')
    p.add_argument('--rotate-model-type', choices=['keras', 'detectron2', 'corr', 'mantle'], default='keras')
    p.add_argument('--rotate-model-dt2-base', 
        default='COCO-Keypoints/keypoint_rcnn_R_50_FPN_3x.yaml')
    p.add_argument('--layer', default='block5_conv1', help='Name of layer.')
    p.add_argument('--mode', choices=['max-pooled', 'gram', 'full'], default='max-pooled')
    p.add_argument('--cuttlefish-radius', type=int, 
        help='Radius around the centre of the cuttlefish that should be cropped.')
    p.add_argument('--min-mask-fraction', type=float, default=0.07,
        help='Minimal fraction of pixels occupied by the cuttlefish mask for radius estimation.')
    p.add_argument('--eq-hist', type=bool, default=False, help='Histogram equalisation.')
    p.add_argument('--ellipse', type=bool, default=False, help='Use elipse mask.')
    p.add_argument('--augmentation', type=str, help='Comma-separated list of \
        "[shift],[angle],[blur]", each parameter "[range:step]". \
        Units: shift - px, angle - degree, blur - kernel size in px. \
        e.g. "100:25,15:3,9:3"')
    p.add_argument('--max-batch-size', default=100, type=int)
    p.add_argument('output', help='Output activations file')
    p.add_argument('--sampling-rate', type=int, help='Sampling rate (every n frames)')
    p.add_argument('--start', type=float, help='Start video at (seconds)')
    p.add_argument('--end', type=float, help='End video at (seconds)')
    p.add_argument('--rerun', action='store_true', help='Overwrite existing output.')
    p.add_argument('--use-gpu', action='store_true', help='Use GPU for pytorch.')

    args = p.parse_args()

    if os.path.isfile(args.output) and not args.rerun:
        raise RuntimeError('Output already exists. Use flag --rerun to overwrite.')

    # Load models in all processes
    pretrained_model = getattr(tf.keras.applications, args.model)(
        weights='imagenet', include_top=False)
    pretrained_model.compile(loss="categorical_crossentropy", optimizer="adam")
    if args.cuttlefish_model_type == 'keras':
        cuttlefish_model = load_model(args.cuttlefish_model)
    elif args.cuttlefish_model_type == 'detectron2':
        cuttlefish_model = util.load_detectron2_model(
            args.cuttlefish_model, args.cuttlefish_model_dt2_base, 
            num_class=2, score_threshold=0.85, use_gpu=args.use_gpu)
    if args.rotate_model_type == 'keras':
        rotate_model = load_model(args.rotate_model)
    elif args.rotate_model_type == 'detectron2':
        rotate_model = util.load_detectron2_model(
            args.rotate_model, args.rotate_model_dt2_base, 
            num_kp=2, score_threshold=0.85, use_gpu=args.use_gpu)
    elif args.rotate_model_type == 'corr':
        rotate_model = None
    elif args.rotate_model_type == 'mantle':
        rotate_model = 'mantle'

    # Parse augmentation string into parameters
    if args.augmentation is None:
        shifts = None
        angles = None
        kernels = None
    else:
        ((shift_range, shift_step),(angle_range, angle_step), (blur_range, blur_step)) = [
                [0 if e == '' else float(e) for e in t.split(':')] 
            for t in args.augmentation.split(',')]
        shifts = np.concatenate([
            np.arange(-shift_range,0,shift_step, dtype='int'), 
            np.arange(shift_range,0,-shift_step, dtype='int')[::-1]])
        angles = np.concatenate([
            np.arange(-angle_range,0,angle_step, dtype='int'), 
            np.arange(angle_range,0,-angle_step, dtype='int')[::-1]])
        kernels = np.arange(0,blur_range,blur_step, dtype='int')[1:]

        aug_params = np.concatenate([
            shifts, shifts, angles, kernels, kernels])
        aug_modes = ['shift_y'] * len(shifts) \
            + ['shift_x'] * len(shifts) \
            + ['angle'] * len(angles) \
            + ['blur_v'] * len(kernels) \
            + ['blur_h'] * len(kernels)

    # If cuttlefish radius is not defined, determine by reading random frames,
    # and such that the frame size is rescaled to double the cuttlefish length.
    if args.cuttlefish_radius is None:
        args.cuttlefish_radius = int(util.get_cuttlefish_length(
            args.video, cuttlefish_model, rotate_model, 
            cuttleifsh_model_type=args.cuttlefish_model_type, 
            upright_model_type=args.rotate_model_type, 
            min_mask_fraction=args.min_mask_fraction, n_samples=5))
    print('cuttlefish_radius:', args.cuttlefish_radius)

    # Set up video info
    cap = cv2.VideoCapture(args.video)
    fps = float(cap.get(cv2.CAP_PROP_FPS))

    if args.start is None:
        frame_start = 0
    else:
        frame_start = args.start * fps

    if args.end is None:
        frame_end = cap.get(cv2.CAP_PROP_FRAME_COUNT)
    else:
        frame_end = args.end * fps

    total_samples = int(np.ceil((frame_end - frame_start) / args.sampling_rate))

    # Set up tmp output file name shared by all processes
    tmp_output = None
    if comm.rank == 0:
        output_dirname = os.path.dirname(args.output)
        if not os.path.isdir(output_dirname):
            os.mkdir(output_dirname)
        output_basename = os.path.basename(args.output)
        suffix = ''.join(random.choice(string.ascii_uppercase + string.digits) \
                for _ in range(4))
        tmp_output_basename = '.' + output_basename + '.' + suffix
        tmp_output = os.path.join(output_dirname, tmp_output_basename)
    tmp_output = comm.bcast(tmp_output, root=0)

    # Get number of features to be saved
    n_features = None
    if comm.rank == 0:
        PBAR = tqdm(total=total_samples, file=sys.stdout)
        if args.mode == 'max-pooled':
            n_features = pretrained_model.get_layer(args.layer).get_output_shape_at(0)[-1]
        elif args.mode == 'gram':
            n_features = pretrained_model.get_layer(args.layer).get_output_shape_at(0)[-1] **2
        elif (args.mode == 'full'):
            # Dummy input for optaining the shape of layer output
            act = keract.get_activations(
                pretrained_model, 
                np.zeros((1, 224, 224, 3)), 
                layer_names=args.layer, auto_compile=True)
            actual_layer_name = list(act.keys())[0]  # Some models add suffix to layer name
            n_features = len(act[actual_layer_name].flatten())
    n_features = comm.bcast(n_features, root=0)

    # Assign a chunk to each worker
    if comm_size == 1:
        samples_chuck_starts = [0]
        samples_chuck_ends = [int(total_samples)]
    else:
        n_chunks = comm_size - 1
        samples_chuck_starts = np.linspace(
            0, total_samples, n_chunks, dtype=int, endpoint=False)
        samples_chuck_ends = np.append(samples_chuck_starts[1:], total_samples)
    sys.stdout.flush()
    # Run main function in all workers
    if (comm_size == 1) or (comm.rank > 0):
        if comm_size == 1:
            chunk_idx = 0
        else:
            chunk_idx = comm.rank - 1
        node_tmp_output = tmp_output + str(chunk_idx)
        get_activations(cap, pretrained_model, args.layer, args.sampling_rate,
            samples_chuck_starts[chunk_idx], samples_chuck_ends[chunk_idx],
            cuttlefish_model, rotate_model, args.cuttlefish_radius, 
            node_tmp_output, args.mode, n_features, eq_hist=args.eq_hist, 
            ellipse=args.ellipse, 
            cuttlefish_model_type=args.cuttlefish_model_type, 
            rotate_model_type=args.rotate_model_type,
            shifts=shifts, angles=angles, kernels=kernels,
            batch_size=args.max_batch_size)

    # Recieve progress bar update in the master node
    if (comm_size > 1) and (comm.rank == 0):
        received = 0
        while received < total_samples:
            update_msg = comm.recv(tag=TQDM_TAG)
            PBAR.update(update_msg)
            PBAR.refresh()
            received += update_msg

    if comm.rank == 0:
        if comm_size == 1:
            rank_start = 0
        else:
            rank_start = 1

        # Set up output file and write parameters
        rel_video_path = os.path.relpath(args.video, os.path.dirname(args.output))
        rel_cuttlefish_model_path = os.path.relpath(
            args.cuttlefish_model, os.path.dirname(args.output))
        if (args.rotate_model_type == 'corr') or (args.rotate_model_type == 'mantle'):
            rel_rotate_model_path = ''
        else:
            rel_rotate_model_path = os.path.relpath(
                args.rotate_model, os.path.dirname(args.output))
        with h5py.File(tmp_output, 'w') as hf:
            hf.attrs['video'] = rel_video_path
            hf.attrs['cuttlefish_model'] = rel_cuttlefish_model_path
            hf.attrs['cuttlefish_model_type'] = args.cuttlefish_model_type
            if args.cuttlefish_model_type == 'detectron2':
                hf.attrs['cuttlefish_model_dt2_base'] = args.cuttlefish_model_dt2_base
            else:
                hf.attrs['cuttlefish_model_dt2_base'] = None
            hf.attrs['rotate_model'] = rel_rotate_model_path
            hf.attrs['rotate_model_type'] = args.rotate_model_type
            if args.cuttlefish_model_type == 'detectron2':
                hf.attrs['rotate_model_dt2_base'] = args.rotate_model_dt2_base
            else:
                hf.attrs['rotate_model_dt2_base'] = None
            hf.attrs['frame_start'] = frame_start
            hf.attrs['frame_end'] = frame_end
            hf.attrs['sampling_rate'] = args.sampling_rate
            hf.attrs['input_length'] = args.cuttlefish_radius*2
            hf.attrs['model'] = args.model
            hf.attrs['layer'] = args.layer
            hf.attrs['mode'] = args.mode
            hf.attrs['eq_hist'] = args.eq_hist
            hf.attrs['ellipse'] = args.ellipse

            hf.create_dataset('areas', (0,), 
                maxshape=(total_samples,), dtype='int32')
            hf.create_dataset('positions', (0, 2),
                maxshape=(total_samples, 2), dtype='float32')
            hf.create_dataset('frame_numbers', (0,),
                maxshape=(total_samples,), dtype='int32')

            hf.create_dataset('activations/original', (0, n_features),
                maxshape=(total_samples, n_features), dtype='float32')

            # Create a dataset under the group activation for each augmentation
            if args.augmentation is not None:
                for mode, value in zip(aug_modes, aug_params):
                    hf.create_dataset('activations/{}{:+}'.format(mode, value), 
                        (0, n_features),
                        maxshape=(total_samples, n_features), dtype='float32')
            
            # Add attributes to all dataset, initialising with 0 for all parameters
            if args.augmentation is not None:
                for key in hf['activations'].keys():
                    for mode in np.unique(aug_modes):
                        hf['activations/'+key].attrs[mode] = 0

            # Rewrite the relavent attributes for each dataset to reflect the 
            # actual augmentation parameter.
            if args.augmentation is not None:
                for mode, value in zip(aug_modes, aug_params):
                    hf['activations/{}{:+}'.format(mode, value)].attrs[mode] = value

        # Collect activation and area outputs from tmp file created by each worker node
        for rank in range(rank_start, comm_size):
            if comm_size == 1:
                chunk_idx = 0
            else:
                chunk_idx = rank - 1
                
            with h5py.File(tmp_output + str(chunk_idx), 'r') as hf:
                act = hf['activations'][:]
                ar = hf['areas'][:]
                coords = hf['positions'][:]
                frames = hf['frame_numbers'][:]

            with h5py.File(tmp_output, 'r+') as hf:
                length, _ = hf['activations/original'].shape

                hf['activations/original'].resize(length + len(act), axis=0)
                hf['activations/original'][-len(act):] = act[:,0].squeeze()

                if args.augmentation is not None:
                    for i, (mode, value) in enumerate(zip(aug_modes, aug_params)):
                        key = 'activations/{}{:+}'.format(mode, value)
                        hf[key].resize(length + len(act), axis=0)
                        hf[key][-len(act):] = act[:,i+1].squeeze()

                hf['areas'].resize(length + len(ar), axis=0)
                hf['areas'][-len(ar):] = ar

                hf['positions'].resize(length + len(coords), axis=0)
                hf['positions'][-len(coords):] = coords

                hf['frame_numbers'].resize(length + len(frames), axis=0)
                hf['frame_numbers'][-len(frames):] = frames

        # Rename output and delete tmp files
        for rank in range(rank_start, comm_size):
            if comm_size == 1:
                chunk_idx = 0
            else:
                chunk_idx = rank - 1
            os.remove(tmp_output + str(chunk_idx))
        os.rename(tmp_output, args.output)